var SEND_TRACE_FREQUENCY = 100;
DrawStack = function() {
    this.stack = [];
    this.traces = [];
    this.ongoingLocalTraces = {};
    this.ongoingRemoteTraces = {};
    this.background = null;
    this.viewports = [{
        ox: 0,
        oy: 0,
        x: 0,
        y: 0,
        zoom: 1
    }];
    this.viewport = {
        ox: 0,
        oy: 0,
        x: 0,
        y: 0,
        zoom: 1
    }
};
DrawStack.fromObject = function(a) {
    var b = new DrawStack;
    $.each(a.stack, function() {
        b.addToFront(DrawableObject.fromObject(this))
    });
    a.background && b.setBackground(Background.fromObject(a.background));
    $.each(a.traces, function() {
        b.addTrace(InkTrace.fromObject(this))
    });
    $.each(a.remoteTraces, function() {
        var a = InkTrace.fromObject(this),
            d = {
                traces: {
                    remote: a
                }
            };
        switch (a.brush.type) {
            case InkBrush.types.drawing:
                d.render = DrawingBrush.prototype.render.bind(d);
                break;
            case InkBrush.types.pointer:
                d.render = PointerBrush.prototype.render.bind(d);
                break;
            case InkBrush.types.stroke_eraser:
                d.render = StrokeEraserBrush.prototype.render.bind(d)
        }
        b.ongoingRemoteTraces[a.id] = d
    });
    return b
};
DrawStack.prototype = {
    addToFront: function(a) {
        this.stack.push(a);
        a instanceof InkTrace && this.addTrace(a)
    },
    addMultiple: function(a) {
        var b = this;
        $.each(a, function() {
            this instanceof Background ? b.setBackground(this) : b.addToFront(this)
        })
    },
    addTrace: function(a) {
        this.traces.push(a)
    },
    drawableInPath: function(a, b, c) {
        a = traceSegToRect(a, b, c);
        for (b = this.stack.length - 1; 0 <= b; b--)
            if (c = this.stack[b], c.intersectsBox(a)) return c;
        return null
    },
    remove: function(a) {
        for (var b = 0; b < this.stack.length; b++)
            if (this.stack[b].id == a) {
                this.stack.splice(b,
                    1);
                break
            }
    },
    removeMultiple: function(a) {
        var b = this;
        $.each(a, function() {
            "background" == this.type ? b.setBackground(null) : b.remove(this.id)
        })
    },
    find: function(a) {
        for (var b = 0; b < this.stack.length; b++)
            if (this.stack[b].id == a) return this.stack[b];
        return null
    },
    clear: function() {
        var a = this.stack;
        this.stack = [];
        return a
    },
    setBackground: function(a) {
        this.background = a
    },
    redrawTopCanvas: function() {
        var a = this;
        $.each(window.fabricCanvases, function(b, c) {
            var d = c.canvas,
                e = c.viewport;
            d.clearContext(d.contextTop);
            for (key in a.ongoingRemoteTraces) a.ongoingRemoteTraces[key].render(d.contextTop,
                e);
            d.freeDrawingBrush.render && $.each(window.fabricCanvases, function(a, b) {
                d.freeDrawingBrush.render(b.canvas.contextTop, b.viewport)
            })
        })
    },
    redraw: function() {
        $(".mathquill-editable").remove();
        $("mathquill").remove();
        var a = this;
        $.each(window.fabricCanvases, function(b, c) {
            var d = c.canvas,
                e = c.viewport;
            d.clear();
            a.background && d.add(a.background.render(e));
            $.each(a.stack, function() {
                var a = this.render(e);
                this._fabricObject = a;
                d.add(a)
            });
            d.renderAll()
        });
        this.redrawTopCanvas()
    },
    getTraceList: function() {
        return this.traces
    },
    updateViewportPos: function(a, b) {
        $.each(window.fabricCanvases, function(c, d) {
            d.viewport.x += a / d.viewport.zoom;
            d.viewport.y += b / d.viewport.zoom
        });
        this.redraw()
    },
    updateViewportZoom: function(a, b, c) {
        $.each(window.fabricCanvases, function(d, e) {
            var f = e.viewport.zoom;
            c || (c = {
                x: e.canvas.width / 2,
                y: e.canvas.height / 2
            });
            e.viewport.zoom = b ? a : e.viewport.zoom + a;
            var g = e.viewport.zoom;.25 > g || 3 < g ? e.viewport.zoom = f : (e.viewport.x += c.x * (1 / f - 1 / g), e.viewport.y += c.y * (1 / f - 1 / g))
        });
        this.redraw()
    },
    toObject: function() {
        var a = {
            stack: [],
            traces: [],
            remoteTraces: []
        };
        $.each(this.stack, function() {
            a.stack.push(this.toObject())
        });
        this.background && (a.background = this.background.toObject());
        $.each(this.traces, function() {
            this.brush.isDrawing() || a.traces.push(this.toObject())
        });
        for (traceID in this.ongoingRemoteTraces) a.remoteTraces.push(this.ongoingRemoteTraces[traceID].traces.remote.toObject());
        return a
    }
};
UndoStack = function(a) {
    this.pageList = a;
    this.actions = [];
    this.pointer = 0
};
UndoStack.prototype = {
    addAction: function(a) {
        this.actions = this.actions.slice(0, this.pointer);
        this.actions.push(a);
        this.pointer++
    },
    undo: function() {
        if (0 < this.pointer) {
            this.pointer--;
            var a = this.actions[this.pointer].undo(this.pageList);
            this.pageList.setActivePage(a.page);
            setPageTextBoxValue();
            return a
        }
    },
    redo: function() {
        if (this.pointer < this.actions.length) {
            var a = this.actions[this.pointer];
            a.perform(this.pageList);
            this.pageList.setActivePage(a.page);
            setPageTextBoxValue();
            this.pointer++;
            return a
        }
    }
};
PageList = function() {
    this._pages = [];
    this._pointer = -1
};
PageList.fromObject = function(a) {
    var b = new PageList;
    $.each(a.pages, function() {
        b._pages.push(DrawStack.fromObject(this))
    });
    return b
};
PageList.prototype = {
    addPage: function(a) {
        this._pages.splice(a, 0, new DrawStack)
    },
    addPageToEnd: function() {
        this.addPage(this.numPages)
    },
    removePage: function(a) {
        this._pages.splice(a - 1, 1);
        0 == this.numPages && this.addPageToEnd();
        a < this.currPage ? this._pointer-- : a == this.currPage && (this.currPage > this.numPages ? this.setActivePage(this.currPage - 1) : this.setActivePage(this.currPage))
    },
    removeCurrPage: function() {
        this.removePage(this.currPage)
    },
    setActivePage: function(a) {
        1 <= a && a <= this.numPages && (this._pointer = a - 1, window.drawStack =
            this._pages[this._pointer], setBrushType(currentBrush.type), window.drawStack.redraw())
    },
    getPage: function(a) {
        return this._pages[a - 1]
    },
    get numPages() {
        return this._pages.length
    },
    get currPage() {
        return this._pointer + 1
    },
    get activeDrawStack() {
        return this._pages[this._pointer]
    },
    toObject: function() {
        var a = [];
        $.each(this._pages, function() {
            a.push(this.toObject())
        });
        return {
            pages: a
        }
    }
};
Action = function() {};
Action.fromObject = function(a) {
    switch (a.type) {
        case "addaction":
            return AddAction.fromObject(a);
        case "removeaction":
            return RemoveAction.fromObject(a);
        case "streamtraceaction":
            return StreamTraceAction.fromObject(a);
        case "modifyaction":
            return ModifyAction.fromObject(a);
        case "modifypagelistaction":
            return ModifyPageListAction.fromObject(a)
    }
};
Action.prototype = {
    perform: function(a) {},
    toObject: function() {
        return this
    }
};
AddAction = function(a, b) {
    this.drawables = a;
    this.page = b;
    this.type = "addaction"
};
AddAction.fromObject = function(a) {
    var b = [];
    $.each(a.drawables, function() {
        b.push(DrawableObject.fromObject(this))
    });
    return new AddAction(b, a.page)
};
AddAction.prototype = $.extend(Object.create(Action.prototype), {
    perform: function(a) {
        var b = a.getPage(this.page);
        b.addMultiple(this.drawables);
        b == a.activeDrawStack && b.redraw()
    },
    undo: function(a) {
        var b = a.getPage(this.page);
        b.removeMultiple(this.drawables);
        b == a.activeDrawStack && b.redraw();
        return new RemoveAction(this.drawables, this.page)
    },
    toObject: function() {
        var a = {
            type: "addaction",
            page: this.page,
            drawables: []
        };
        $.each(this.drawables, function() {
            a.drawables.push(this.toObject())
        });
        return a
    }
});
RemoveAction = function(a, b, c) {
    this.isClear = c;
    this.page = b;
    this.drawables = a;
    this.type = "removeaction"
};
RemoveAction.fromObject = function(a) {
    var b = [];
    a.drawables && $.each(a.drawables, function() {
        b.push(DrawableObject.fromObject(this))
    });
    return new RemoveAction(b, a.page, a.isClear)
};
RemoveAction.prototype = $.extend(Object.create(Action.prototype), {
    perform: function(a) {
        var b = a.getPage(this.page);
        this.isClear ? b.clear() : b.removeMultiple(this.drawables);
        b == a.activeDrawStack && b.redraw()
    },
    undo: function(a) {
        var b = a.getPage(this.page);
        b.addMultiple(this.drawables);
        b == a.activeDrawStack && b.redraw();
        return new AddAction(this.drawables, this.page)
    },
    toObject: function() {
        var a = {
            type: "removeaction",
            page: this.page,
            drawables: []
        };
        this.isClear ? a.isClear = !0 : $.each(this.drawables, function() {
            "background" ==
            this.type ? a.drawables.push({
                type: "background"
            }) : a.drawables.push({
                id: this.id
            })
        });
        return a
    }
});
StreamTraceAction = function(a, b, c) {
    this.traceID = a;
    this.values = b;
    this.page = c;
    this.type = "streamtraceaction"
};
StreamTraceAction.fromObject = function(a) {
    return new StreamTraceAction(a.traceID, a.values, a.page)
};
StreamTraceAction.prototype = $.extend(Object.create(Action.prototype), {
    perform: function(a) {
        for (var b = a.getPage(this.page), c = b.ongoingRemoteTraces; this.values.length;) {
            var d = this.values.shift();
            if (d.type) {
                InkBrush.fromObject(d);
                var e = this.values.shift(),
                    e = InkTrace.fromDrawing(e, d);
                e.id = this.traceID;
                e = {
                    traces: {
                        remote: e
                    }
                };
                switch (d.type) {
                    case InkBrush.types.drawing:
                        e.render = DrawingBrush.prototype.render.bind(e);
                        break;
                    case InkBrush.types.pointer:
                        e.render = PointerBrush.prototype.render.bind(e);
                        break;
                    case InkBrush.types.stroke_eraser:
                        e.render =
                            StrokeEraserBrush.prototype.render.bind(e)
                }
                c[this.traceID] = e
            } else {
                if ("done" == d) {
                    d = c[this.traceID].traces.remote;
                    d.completeTrace();
                    delete c[this.traceID];
                    d.brush.type == InkBrush.types.drawing ? (b.addToFront(d), b == a.activeDrawStack && b.redraw()) : b.addTrace(d);
                    return
                }
                e = c[this.traceID];
                e = e.traces.remote;
                e.addPoint(d)
            }
        }
        b == a.activeDrawStack && b.redrawTopCanvas()
    }
});
ModifyAction = function(a, b, c) {
    this.id = a;
    this.properties = b;
    this.page = c;
    this.type = "modifyaction"
};
ModifyAction.fromObject = function(a) {
    return new ModifyAction(a.id, a.properties, a.page)
};
ModifyAction.prototype = $.extend(Object.create(Action.prototype), {
    perform: function(a) {
        var b = a.getPage(this.page),
            c = b.find(this.id);
        if (c)
            for (key in this.properties) c[key] = this.properties[key];
        c.updateBoundingBox();
        b == a.activeDrawStack && b.redraw()
    }
});
ModifyPageListAction = function(a, b) {
    this.actionType = a;
    this.page = b;
    this.type = "modifypagelistaction"
};
ModifyPageListAction.fromObject = function(a) {
    return new ModifyPageListAction(a.actionType, a.page)
};
ModifyPageListAction.prototype = $.extend(Object.create(Action.prototype), {
    perform: function(a) {
        switch (this.actionType) {
            case "add":
                a.addPage(this.page);
                break;
            case "remove":
                a.removePage(this.page)
        }
        setPageTextBoxValue()
    }
});
DrawableObject = function() {
    this.ypos = this.xpos = 0;
    this.scaleY = this.scaleX = 1;
    this.angle = 0;
    this.id = genGUID()
};
DrawableObject.fromObject = function(a) {
    switch (a.type) {
        case "inktrace":
            return InkTrace.fromObject(a);
        case "background":
            return Background.fromObject(a);
        case "image":
            return CanvasImage.fromObject(a);
        case "textbox":
            return TextBox.fromObject(a);
        case "mathquillbox":
            return MathQuillBox.fromObject(a);
        default:
            return a
    }
};
DrawableObject.prototype = {
    render: function(a) {},
    updateBoundingBox: function() {
        var a = this.render().setCoords().oCoords;
        this._boundingBox = [a.tl, a.tr, a.br, a.bl]
    },
    intersectsBox: function(a) {
        this._boundingBox || this.updateBoundingBox();
        return boxesIntersect(this._boundingBox, a) ? !0 : !1
    },
    updateCoordsFromFabric: function(a) {
        if (this._fabricObject) {
            var b = this._fabricObject,
                c = this._fabricObject.group;
            if (c) var d = c._getRotatedLeftTop(b),
                b = {
                    xpos: c.getLeft() + d.left,
                    ypos: c.getTop() + d.top,
                    scaleX: c.scaleX * b.scaleX,
                    scaleY: c.scaleY *
                        b.scaleY,
                    angle: c.getAngle() + b.getAngle()
                };
            else c = b.translateToOriginPoint(b.getCenterPoint(), "left", "top"), b = {
                xpos: c.x,
                ypos: c.y,
                scaleX: b.scaleX,
                scaleY: b.scaleY,
                angle: b.angle
            };
            a && (b.xpos = b.xpos / a.zoom + a.x, b.ypos = b.ypos / a.zoom + a.y, b.scaleX /= a.zoom, b.scaleY /= a.zoom);
            $.extend(this, b);
            this.updateBoundingBox();
            return b
        }
    },
    getFabricOptions: function(a) {
        var b = {
            left: this.xpos ? this.xpos : 0,
            top: this.ypos ? this.ypos : 0,
            scaleX: this.scaleX ? this.scaleX : 1,
            scaleY: this.scaleY ? this.scaleY : 1,
            angle: this.angle ? this.angle : 0
        };
        a && (b.left = (b.left - a.x) * a.zoom, b.top = (b.top - a.y) * a.zoom, b.scaleX *= a.zoom, b.scaleY *= a.zoom);
        return b
    },
    toObject: function() {
        var a = {};
        for (key in this) this.hasOwnProperty(key) && "_" != key[0] && (a[key] = this[key]);
        return a
    }
};
CanvasImage = function(a, b) {
    DrawableObject.call(this);
    this.type = "image";
    this.url = a;
    var c = this;
    fabric.util.loadImage(a, function(a) {
        c._image = a;
        drawStack.find(c.id) && drawStack.redraw()
    }, null, "anonymous");
    $.extend(this, b)
};
CanvasImage.fromObject = function(a) {
    return new CanvasImage(a.url, a)
};
CanvasImage.prototype = $.extend(Object.create(DrawableObject.prototype), {
    render: function(a) {
        return this._image ? (a = new fabric.Image(this._image, this.getFabricOptions(a)), a.parentObject = this, a) : new fabric.Object
    }
});
TextBox = function(a) {
    DrawableObject.call(this);
    this.type = "textbox";
    this.fontOptions = {};
    this.setFontOptions(window.fontOptions);
    this.text = "";
    $.extend(this, a)
};
TextBox.fromObject = function(a) {
    return new TextBox(a)
};
TextBox.prototype = $.extend(Object.create(DrawableObject.prototype), {
    setFontOptions: function(a) {
        for (var b in a) this.fontOptions[b] = a[b]
    },
    getFontOptions: function(a) {
        for (var b in this.fontOptions) a[b] = this.fontOptions[b];
        return a
    },
    render: function(a) {
        a = this.getFabricOptions(a);
        $.extend(a, this.fontOptions);
        a = new fabric.IText(this.text, a);
        a.parentObject = this;
        return a
    },
    setActive: function() {
        this._fabricObject && (fabricCanvas.setActiveObject(this._fabricObject), this._fabricObject.enterEditing())
    },
    getFabricOptions: function(a) {
        return $.extend({
            fontFamily: this.font,
            fill: this.color
        }, DrawableObject.prototype.getFabricOptions.call(this, a))
    }
});
MathQuillBox = function(a) {
    DrawableObject.call(this);
    this.type = "mathquillbox";
    this.latex = "";
    $.extend(this, a)
};
MathQuillBox.fromObject = function(a) {
    return new MathQuillBox(a)
};
MathQuillBox.prototype = $.extend(Object.create(DrawableObject.prototype), {
    render: function(a) {
        this._mqObject = a = $("<span/>");
        "" == this.latex ? a.appendTo("#sketch").mathquill("editable") : a.appendTo("#sketch").mathquill("editable").mathquill("latex", this.latex);
        a.removeClass("hasCursor");
        this.setMQPosition();
        a.css("color", this.color);
        a.css("border-color", this.color);
        var b = new fabric.Rect(this.getFabricOptions(window.fabricCanvases[0].viewport));
        b.parentObject = this;
        var c = this;
        a.keydown(function() {
            setTimeout(function() {
                b.set(c.getMQWidthHeight());
                b.setCoords();
                c.updateBoundingBox();
                fabricCanvas.fire("math:changed", {
                    target: c
                })
            })
        });
        a.bind("mousedown.mathquill", function() {
            c.setActive()
        });
        b.on("moving", function() {
            c.updateCoordsFromFabric(window.fabricCanvases[0].viewport);
            c.setMQPosition()
        });
        b.on("rotating", function() {
            c.updateCoordsFromFabric(window.fabricCanvases[0].viewport);
            c.setMQPosition()
        });
        return b
    },
    setActive: function(a) {
        fabricCanvas.deactivateAll().renderAll();
        isEditingMathQuill = !0;
        a && a.trigger("mousedown.mathquill")
    },
    getFabricOptions: function(a) {
        return $.extend({
            fill: null,
            lockScalingX: !0,
            lockScalingY: !0,
            lockScalingUni: !0,
            padding: 5
        }, this.getMQWidthHeight(), DrawableObject.prototype.getFabricOptions.call(this, a))
    },
    getMQWidthHeight: function() {
        return {
            width: this._mqObject.width(),
            height: this._mqObject.height()
        }
    },
    updateBoundingBox: function() {
        var a = (new fabric.Rect(this.getFabricOptions())).setCoords().oCoords;
        this._boundingBox = [a.tl, a.tr, a.br, a.bl]
    },
    setMQPosition: function() {
        var a = this.getFabricOptions(window.fabricCanvases[0].viewport);
        this._mqObject.css({
            position: "absolute",
            top: a.top + "px",
            left: a.left + "px",
            transform: "rotate(" + a.angle + "deg)",
            "transform-origin": "left top",
            "-webkit-transform": "rotate(" + a.angle + "deg)",
            "-webkit-transform-origin": "left top"
        })
    },
    getLatexContents: function() {
        return this._mqObject.mathquill("latex")
    }
});
ContextCapturer = function() {
    var a = document.createElement("canvas").getContext("2d");
    this.capture = [];
    var b = this;
    $.each(a, function(c) {
        "measureText" == c ? b[c] = a[c].bind(a) : "canvas" == c ? b[c] = a[c] : "function" == typeof a[c] ? b[c] = function() {
            b.capture.push({
                type: "call",
                target: c,
                args: arguments
            });
            a[c].apply(a, arguments)
        } : Object.defineProperty(b, c, {
            enumerable: !0,
            configurable: !0,
            set: function(d) {
                b.capture.push({
                    type: "set",
                    target: c,
                    val: d
                });
                a[c] = d
            }
        })
    })
};
FabricCaptureRendererObject = fabric.util.createClass(fabric.Object, {
    initialize: function(a, b) {
        b = b || {};
        b.selectable = !1;
        this.callSuper("initialize", b);
        this.capture = a
    },
    render: function(a) {
        a.save();
        a.translate(this.left, this.top);
        a.rotate(degreesToRadians(this.angle));
        a.scale(this.scaleX * (this.flipX ? -1 : 1), this.scaleY * (this.flipY ? -1 : 1));
        $.each(this.capture, function() {
            "call" == this.type ? a[this.target].apply(a, this.args) : "set" == this.type && (a[this.target] = this.val)
        });
        a.restore()
    }
});
Background = function(a, b) {
    this.type = "background";
    this.style = a;
    $.extend(this, b);
    if (!this._content) {
        var c = this;
        if (3 == this.style || 4 == this.style || 5 == this.style) fabric.util.loadImage(this.url, function(a) {
            c._content = a;
            drawStack.background == c && drawStack.redraw()
        }, null, "anonymous");
        else if (6 == this.style) {
            var d = this.url;
            0 == this.url.indexOf("data:") && (d = dataURLToBuffer(this.url));
            this.src = d
        }
    }
};
Background.fromObject = function(a) {
    return new Background(a.style, a)
};
Background.prototype = {
    render: function(a) {
        var b = fabricCanvas.width,
            c = fabricCanvas.height,
            d = DrawableObject.prototype.getFabricOptions(a),
            e = new fabric.Group([]);
        switch (this.style) {
            case 0:
                for (var f = 40 * a.zoom, g = d.left % f; g < b; g += f) {
                    var h = new fabric.Line([0, 0, 0, c], {
                        left: g,
                        top: 0,
                        selectable: !1,
                        stroke: "#83C6F2",
                        strokeWidth: a.zoom
                    });
                    e.add(h)
                }
                for (d = d.top % f; d < c; d += f) g = new fabric.Line([0, 0, b, 0], {
                    left: 0,
                    top: d,
                    selectable: !1,
                    stroke: "#83C6F2",
                    strokeWidth: a.zoom
                }), e.add(g);
                break;
            case 1:
                for (var f = 10 * a.zoom, g = 20 * a.zoom,
                        l = 5 * f + g, h = 0, d = d.top % l - l; d < c; d += f) l = new fabric.Line([0, 0, b, 0], {
                    left: 0,
                    top: d,
                    selectable: !1,
                    stroke: "black",
                    strokeWidth: a.zoom
                }), h++, 0 == h % 5 && (d += g), e.add(l);
                break;
            case 2:
                f = 40 * a.zoom;
                for (d = d.top % f; d < c; d += f) g = new fabric.Line([0, 0, b, 0], {
                    left: 0,
                    top: d,
                    selectable: !1,
                    stroke: "#83C6F2",
                    strokeWidth: a.zoom
                }), e.add(g);
                break;
            case 3:
                if (this._content) return new fabric.Image(this._content, $.extend(d, {
                    selectable: !1
                }));
                break;
            case 4:
                if (this._content) return e = new fabric.Pattern({
                    source: this._content,
                    repeat: "repeat",
                    offsetX: d.left /
                        a.zoom,
                    offsetY: d.top / a.zoom
                }), new fabric.Rect({
                    width: b / a.zoom,
                    height: c / a.zoom,
                    scaleX: a.zoom,
                    scaleY: a.zoom,
                    fill: e,
                    selectable: !1
                });
                break;
            case 5:
                if (this._content) return new fabric.Image(this._content, $.extend(d, {
                    height: this.height,
                    width: this.width,
                    selectable: !1
                }));
                break;
            case 6:
                if (this._content) return new FabricCaptureRendererObject(this._content, $.extend(d, {
                    selectable: !1
                }));
                var m = new ContextCapturer,
                    n = this;
                this.pdfPage.render({
                    canvasContext: m,
                    viewport: n.pdfPage.getViewport(1)
                }).then(function() {
                    n._content =
                        m.capture;
                    drawStack.redraw()
                });
                break;
            case 7:
                f = 170 * a.zoom;
                b = d.left + 100 * a.zoom;
                c = d.top + 100 * a.zoom;
                for (g = 0; g < 2 * f; g += f) d = new fabric.Line([0, 0, 0, 3 * f], {
                    left: b + g + f,
                    top: c,
                    selectable: !1,
                    stroke: "#000",
                    strokeWidth: 3 * a.zoom
                }), e.add(d);
                for (d = 0; d < 2 * f; d += f) g = new fabric.Line([0, 0, 3 * f, 0], {
                    left: b,
                    top: c + d + f,
                    selectable: !1,
                    stroke: "#000",
                    strokeWidth: 3 * a.zoom
                }), e.add(g)
        }
        return e
    },
    toObject: DrawableObject.prototype.toObject
};

function pointToViewportCoords(a, b) {
    a.x = a.x / b.zoom + b.x;
    a.y = a.y / b.zoom + b.y;
    return a
}

function touchToPoint(a, b, c) {
    b = {
        x: a.clientX - b.ox,
        y: a.clientY - b.oy
    };
    a.webkitForce ? b.f = a.webkitForce : a.force && (b.f = a.force);
    return b
}
DrawingBrush = function(a) {
    this.viewport = a;
    this.brush = currentBrush;
    this.traces = {};
    this.sendCache = {};
    var b = this;
    this.timerID = setInterval(function() {
        for (traceID in b.sendCache) {
            var a = new StreamTraceAction(traceID, b.sendCache[traceID], pageList.currPage);
            window.commInterface && commInterface.sendAction(a);
            delete b.sendCache[traceID]
        }
    }, SEND_TRACE_FREQUENCY)
};
DrawingBrush.prototype = {
    _handleTraceCreate: function(a, b) {
        b = pointToViewportCoords(b, this.viewport);
        var c = InkTrace.fromDrawing(b, this.brush);
        this.traces[a] = c;
        this.sendCache[c.id] = [this.brush, cloneObject(b)]
    },
    onMouseDown: function(a) {
        if (a.touches) {
            var b = this;
            $.each(a.touches, function() {
                b._handleTraceCreate(this.identifier, touchToPoint(this, b.viewport))
            })
        } else this._handleTraceCreate("mouse", a);
        drawStack.redrawTopCanvas()
    },
    _handleTracePoint: function(a, b) {
        b = pointToViewportCoords(b, this.viewport);
        var c =
            this.traces[a];
        c.addPoint(b);
        this.sendCache[c.id] || (this.sendCache[c.id] = []);
        this.sendCache[c.id].push(cloneObject(b))
    },
    onMouseMove: function(a) {
        if (a.touches) {
            var b = this;
            $.each(a.touches, function() {
                b._handleTracePoint(this.identifier, touchToPoint(this, b.viewport))
            })
        } else this._handleTracePoint("mouse", a);
        drawStack.redrawTopCanvas()
    },
    _handleTraceFinish: function(a) {
        var b = this.traces[a];
        delete this.traces[a];
        b.completeTrace();
        drawStack.addToFront(b);
        drawStack.redraw();
        a = new AddAction([b], pageList.currPage);
        undoStack.addAction(a);
        this.sendCache[b.id] || (this.sendCache[b.id] = []);
        this.sendCache[b.id].push("done")
    },
    onMouseUp: function(a) {
        if (a) {
            var b = this;
            $.each(a, function() {
                b._handleTraceFinish(this.identifier)
            })
        } else this._handleTraceFinish("mouse")
    },
    render: function(a, b) {
        for (traceID in this.traces) this.traces[traceID].render(b).render(a)
    },
    dispose: function() {
        clearInterval(this.timerID)
    }
};
LineBrush = function(a) {
    this.viewport = a;
    this.brush = currentBrush;
    this.traces = {}
};
LineBrush.prototype = {
    onMouseDown: function(a) {
        if (a.touches) {
            var b = this;
            $.each(a.touches, function() {
                var a = pointToViewportCoords(touchToPoint(this, b.viewport), b.viewport);
                b.traces[this.identifier] = InkTrace.fromDrawing(a, b.brush)
            })
        } else a = pointToViewportCoords(a, this.viewport), this.traces.mouse = InkTrace.fromDrawing(a, this.brush);
        drawStack.redrawTopCanvas()
    },
    onMouseMove: function(a) {
        if (a.touches) {
            var b = this;
            $.each(a.touches, function() {
                var a = pointToViewportCoords(touchToPoint(this, b.viewport), b.viewport);
                b.traces[this.identifier].replaceLastPoint(a)
            })
        } else a = pointToViewportCoords(a, this.viewport), this.traces.mouse.replaceLastPoint(a);
        drawStack.redrawTopCanvas()
    },
    _handleTraceFinish: function(a) {
        var b = this.traces[a];
        delete this.traces[a];
        1 < b.values.length ? (b.completeTrace(), drawStack.addToFront(b), drawStack.redraw(), a = new AddAction([b], pageList.currPage), undoStack.addAction(a), window.commInterface && commInterface.sendAction(a)) : drawStack.redrawTopCanvas()
    },
    onMouseUp: function(a) {
        if (a) {
            var b = this;
            $.each(a,
                function() {
                    b._handleTraceFinish(this.identifier)
                })
        } else this._handleTraceFinish("mouse")
    },
    render: function(a, b) {
        for (traceID in this.traces) this.traces[traceID].render(b).render(a)
    }
};
StrokeEraserBrush = function(a) {
    this.viewport = a;
    this.brush = currentBrush;
    this.traces = {};
    this.sendCache = {};
    var b = this;
    this.timerID = setInterval(function() {
        for (traceID in b.sendCache) {
            var a = new StreamTraceAction(traceID, b.sendCache[traceID], pageList.currPage);
            window.commInterface && commInterface.sendAction(a);
            delete b.sendCache[traceID]
        }
    }, SEND_TRACE_FREQUENCY)
};
StrokeEraserBrush.prototype = {
    _handleTraceCreate: function(a, b) {
        b = pointToViewportCoords(b, this.viewport);
        var c = InkTrace.fromDrawing(b, this.brush);
        this._performErase(b, b, this.brush);
        this.traces[a] = c;
        this.sendCache[c.id] = [this.brush, cloneObject(b)]
    },
    onMouseDown: function(a) {
        if (a.touches) {
            var b = this;
            $.each(a.touches, function() {
                b._handleTraceCreate(this.identifier, touchToPoint(this, b.viewport))
            })
        } else this._handleTraceCreate("mouse", a)
    },
    _handleTracePoint: function(a, b) {
        b = pointToViewportCoords(b, this.viewport);
        var c = this.traces[a];
        this._performErase(b, c.getLastPoint(), c.brush);
        c.addPoint(b);
        this.sendCache[c.id] || (this.sendCache[c.id] = []);
        this.sendCache[c.id].push(cloneObject(b))
    },
    onMouseMove: function(a) {
        if (a.touches) {
            var b = this;
            $.each(a.touches, function() {
                b._handleTracePoint(this.identifier, touchToPoint(this, b.viewport))
            })
        } else this._handleTracePoint("mouse", a)
    },
    _performErase: function(a, b, c) {
        if (a = drawStack.drawableInPath(b, a, c.width)) drawStack.remove(a.id), drawStack.redraw(), a = new RemoveAction([a], pageList.currPage),
            undoStack.addAction(a), window.commInterface && commInterface.sendAction(a)
    },
    _handleTraceFinish: function(a) {
        var b = this.traces[a];
        delete this.traces[a];
        b.completeTrace();
        drawStack.addTrace(b);
        this.sendCache[b.id] || (this.sendCache[b.id] = []);
        this.sendCache[b.id].push("done")
    },
    onMouseUp: function(a) {
        if (a) {
            var b = this;
            $.each(a, function() {
                b._handleTraceFinish(this.identifier)
            })
        } else this._handleTraceFinish("mouse")
    },
    render: new Function,
    dispose: function() {
        clearInterval(this.timerID)
    }
};
PointerBrush = function(a) {
    this.viewport = a;
    this.brush = currentBrush;
    this.traces = {};
    var b = this;
    fabric.isTouchSupported ? (this.onMouseDown = function(a) {
        $.each(a.touches, function() {
            b._handleTraceCreate(this.identifier, touchToPoint(this, b.viewport))
        })
    }, this.onMouseMove = function(a) {
        $.each(a.touches, function() {
            b._handleTracePoint(this.identifier, touchToPoint(this, b.viewport))
        })
    }, this.onMouseUp = function(a) {
        $.each(a, function() {
            b._handleTraceFinish(this.identifier)
        })
    }) : (this.onMouseDown = function() {
            drawStack.redrawTopCanvas()
        },
        this.onMouseMove = new Function, this.onMouseUp = new Function, fabricCanvas.on("mouse:move", function(a) {
            a = fabric.util.getPointer(a.e);
            b._handleTracePoint("mouse", a)
        }), $(fabricCanvas.wrapperEl).on("mouseleave", function() {
            b._handleTraceFinish("mouse")
        }));
    this.sendCache = {};
    b = this;
    this.timerID = setInterval(function() {
        for (traceID in b.sendCache) {
            var a = new StreamTraceAction(traceID, b.sendCache[traceID], pageList.currPage);
            window.commInterface && commInterface.sendAction(a);
            delete b.sendCache[traceID]
        }
    }, SEND_TRACE_FREQUENCY)
};
PointerBrush.prototype = {
    _handleTraceCreate: function(a, b) {
        b = pointToViewportCoords(b, this.viewport);
        var c = InkTrace.fromDrawing(b, this.brush);
        this.traces[a] = c;
        drawStack.redrawTopCanvas();
        this.sendCache[c.id] = [this.brush, cloneObject(b)]
    },
    _handleTracePoint: function(a, b) {
        var c = this.traces[a];
        c ? (b = pointToViewportCoords(b, this.viewport), c.addPoint(b), drawStack.redrawTopCanvas(), this.sendCache[c.id] || (this.sendCache[c.id] = []), this.sendCache[c.id].push(cloneObject(b))) : this._handleTraceCreate(a, b)
    },
    _handleTraceFinish: function(a) {
        var b =
            this.traces[a];
        delete this.traces[a];
        b.completeTrace();
        drawStack.addTrace(b);
        drawStack.redrawTopCanvas();
        this.sendCache[b.id] || (this.sendCache[b.id] = []);
        this.sendCache[b.id].push("done")
    },
    render: function(a, b) {
        for (traceID in this.traces) {
            var c = this.traces[traceID].getLastPoint();
            a.lineWidth = 5;
            a.strokeStyle = "#FF0000";
            a.beginPath();
            a.arc((c.x - b.x) * b.zoom, (c.y - b.y) * b.zoom, 10, 0, 2 * Math.PI);
            a.stroke()
        }
    },
    dispose: function() {
        clearInterval(this.timerID);
        fabric.isTouchSupported || (fabricCanvas.off("mouse:move"),
            $(fabricCanvas.wrapperEl).off("mouseleave"))
    }
};
TextBrush = function() {
    this.brush = currentBrush;
    this.viewport = fabricCanvases[0].viewport;
    var a = this,
        b = !1;
    fabricCanvas.on("object:selected", function() {
        b = !0
    });
    fabricCanvas.on("mouse:down", function(c) {
        fabricCanvas._isCurrentlyDrawing = !1;
        var d = fabricCanvas.findTarget(c.e),
            e = pointToViewportCoords(fabric.util.getPointer(c.e), a.viewport),
            f = d instanceof fabric.IText;
        b || f ? f ? (fabricCanvas.isDrawingMode = !1, fabricCanvas.setActiveObject(d), d.setCursorByClick(c.e), d.enterEditing()) : (fabricCanvas.isDrawingMode = !0,
            fabricCanvas.deactivateAll(), fabricCanvas.renderAll()) : (fabricCanvas.isDrawingMode = !1, c = new TextBox({
            xpos: e.x - 4,
            ypos: e.y - 30,
            color: a.brush.color
        }), drawStack.addToFront(c), drawStack.redraw(), c.setActive(), c = new AddAction([c], pageList.currPage), window.commInterface && commInterface.sendAction(c));
        fabricCanvas.getActiveObject() || (b = !1)
    })
};
TextBrush.prototype = {
    onMouseDown: new Function,
    onMouseMove: new Function,
    onMouseUp: new Function,
    render: new Function,
    dispose: function() {
        fabricCanvas.off("object:selected");
        fabricCanvas.off("mouse:down");
        fabricCanvas.on("mouse:down", hideAllMenuControlBars);
        var a = fabricCanvas.getActiveObject();
        a instanceof fabric.IText && setTimeout(function() {
            a.exitEditing()
        });
        fabricCanvas.deactivateAll();
        fabricCanvas.renderAll()
    }
};
var isEditingMathQuill;
MathBrush = function() {
    this.brush = currentBrush;
    this.viewport = fabricCanvases[0].viewport;
    var a = this,
        b, c = !1;
    fabricCanvas.on("object:selected", function() {
        c = !0
    });
    isEditingMathQuill = !1;
    fabricCanvas.on("mouse:down", function(d) {
        fabricCanvas._isCurrentlyDrawing = !1;
        var e = fabricCanvas.findTarget(d.e);
        d = pointToViewportCoords(fabric.util.getPointer(d.e), a.viewport);
        var f = e instanceof fabric.Rect;
        c || f || isEditingMathQuill ? f ? (isEditingMathQuill = !1, fabricCanvas.isDrawingMode = !1, fabricCanvas.setActiveObject(e)) : (isEditingMathQuill = !1, fabricCanvas.isDrawingMode = !0, fabricCanvas.deactivateAll(), fabricCanvas.renderAll(), fabricCanvas.fire("math:editing:exited", {
            target: b
        })) : (fabricCanvas.isDrawingMode = !1, b = new MathQuillBox({
            xpos: d.x,
            ypos: d.y - 15,
            color: a.brush.color
        }), drawStack.addToFront(b), drawStack.redraw(), b.setActive(b._mqObject), e = new AddAction([b], pageList.currPage), window.commInterface && commInterface.sendAction(e));
        fabricCanvas.getActiveObject() || (c = !1);
        a._mqBox = b
    })
};
MathBrush.prototype = {
    onMouseDown: new Function,
    onMouseMove: new Function,
    onMouseUp: new Function,
    render: new Function,
    dispose: function() {
        fabricCanvas.off("object:selected");
        fabricCanvas.off("mouse:down");
        fabricCanvas.on("mouse:down", hideAllMenuControlBars);
        isEditingMathQuill && fabricCanvas.fire("math:editing:exited", {
            target: this._mqBox
        });
        fabricCanvas.deactivateAll();
        fabricCanvas.renderAll()
    }
};
SelectionBrush = function() {
    hammer.get("pinch").set({
        enable: !0
    });
    hammer.get("rotate").set({
        enable: !0
    });
    hammer.on("pinchstart", function(a) {
        console.log(a)
    });
    hammer.on("pinch", function(a) {
        a.srcEvent.stopPropagation();
        console.log("pinched from s");
        a = traceSegToRect(a.center, a.center, 1);
        var b = fabricCanvas.getActiveObject() || fabricCanvas.getActiveGroup();
        b && boxesIntersect(a, [b.oCoords.tl, b.oCoords.tr, b.oCoords.br, b.oCoords.bl]) && console.log("intersect")
    });
    hammer.on("rotate", function(a) {
        a.srcEvent.stopPropagation();
        console.log("rotated from s ", a)
    })
};
SelectionBrush.prototype = {
    render: new Function,
    dispose: function() {
        hammer.off("pinch");
        hammer.off("rotate");
        hammer.get("pinch").set({
            enable: !1
        });
        hammer.get("rotate").set({
            enable: !1
        });
        fabricCanvas.deactivateAll();
        fabricCanvas.renderAll()
    }
};
ViewportBrush = function(a) {
    fabricCanvas.freeDrawingCursor = "move";
    this.viewport = a;
    hammer.get("pan").set({
        enable: !0,
        pointers: 1
    });
    hammer.get("pinch").set({
        enable: !0
    });
    hammer.get("rotate").set({
        enable: !0
    });
    var b;
    hammer.on("panstart", function(a) {
        b = a.center
    });
    hammer.on("panmove", function(c) {
        drawStack.updateViewportPos(b.x - c.center.x, b.y - c.center.y);
        window.canvasPositionDebug && (window.canvasPositionDebug.innerHTML = "x: " + a.x + "y: " + a.y);
        b = c.center
    });
    var c;
    hammer.on("pinchstart", function() {
        c = 1
    });
    hammer.on("pinch",
        function(a) {
            drawStack.updateViewportZoom(a.scale - c, !1, a.center);
            setZoomTextBoxValue();
            c = a.scale
        });
    $(fabricCanvas.wrapperEl).on("wheel", function(a) {
        a = a.originalEvent;
        var b = {
            x: a.clientX,
            y: a.clientY
        };
        0 < a.wheelDelta ? (drawStack.updateViewportZoom(.25, !1, b), setZoomTextBoxValue()) : 0 > a.wheelDelta && (drawStack.updateViewportZoom(-.25, !1, b), setZoomTextBoxValue())
    })
};
ViewportBrush.prototype = {
    onMouseDown: new Function,
    onMouseMove: new Function,
    onMouseUp: new Function,
    render: new Function,
    dispose: function() {
        fabricCanvas.freeDrawingCursor = "crosshair";
        hammer.off("pinch");
        hammer.off("rotate");
        hammer.off("panstart");
        hammer.off("panmove");
        hammer.get("pan").set({
            enable: !0,
            pointers: 2
        });
        hammer.get("pinch").set({
            enable: !1
        });
        hammer.get("rotate").set({
            enable: !1
        });
        $(fabricCanvas.wrapperEl).off("wheel")
    }
};

function initCanvas(a, b, c, d) {
    a = new fabric.Canvas(a);
    a.renderOnAddRemove = !1;
    a.setWidth(b);
    a.setHeight(c);
    a.setBackgroundColor("white");
    var e = window.fabricCanvases.push({
        canvas: a,
        viewport: d
    }) - 1;
    a.on("mouse:down", hideAllMenuControlBars);
    a.on("object:modified", function(a) {
        if (a.target instanceof fabric.Group) $.each(a.target.getObjects(), function() {
            var a = this.parentObject,
                b = a.updateCoordsFromFabric(window.fabricCanvases[e].viewport);
            sendChange(a.id, b)
        });
        else {
            a = a.target.parentObject;
            var b = a.updateCoordsFromFabric(window.fabricCanvases[e].viewport);
            sendChange(a.id, b)
        }
    });
    a.on("text:changed", function(a) {
        a = a.target;
        var b = a.parentObject;
        b.text = a.text;
        b.updateBoundingBox();
        sendChange(b.id, {
            text: a.text,
            fontOptions: b.fontOptions
        })
    });
    a.on("text:editing:entered", function(a) {
        a = a.target;
        var b = a.parentObject;
        b instanceof TextBox && (window.fontOptions = b.getFontOptions(window.fontOptions), window.allControlBars.textSettingsMain.xpos = a.left, window.allControlBars.textSettingsMain.ypos = a.top - window.allControlBars.textSettingsMain.height, window.allControlBars.textSettingsMain.initShow())
    });
    a.on("text:editing:exited", function(a) {
        window.allControlBars.textSettingsMain.destroy();
        a = a.target;
        var b = a.parentObject;
        sendChange(b.id, {
            text: a.text,
            fontOptions: b.fontOptions
        });
        0 == a.text.length && (drawStack.remove(b.id), drawStack.redraw(), a = new RemoveAction([b], pageList.currPage), window.commInterface && commInterface.sendAction(a))
    });
    a.on("math:changed", function(a) {
        a = a.target;
        a.latex = a._mqObject.mathquill("latex");
        a.updateBoundingBox();
        sendChange(a.id, {
            latex: a.latex
        })
    });
    a.on("math:editing:exited", function(a) {
        a =
            a.target;
        0 == a.latex.length && (drawStack.remove(a.id), drawStack.redraw(), a = new RemoveAction([a], pageList.currPage), window.commInterface && commInterface.sendAction(a))
    });
    hammer = null;
    reInitMainCanvasListeners(a);
    return a
}

function reInitMainCanvasListeners(a) {
    null != hammer && hammer.destroy();
    hammer = new Hammer.Manager(a.wrapperEl, {
        recognizers: [
            [Hammer.Pan, {
                enable: !0,
                pointers: 2
            }],
            [Hammer.Pinch, {
                enable: !1,
                threshold: .04
            }],
            [Hammer.Rotate, {
                enable: !1,
                threshold: 3
            }],
            [Hammer.Press, {
                enable: !1
            }],
            [Hammer.Swipe, {
                enable: !1
            }],
            [Hammer.Tap, {
                enable: !1
            }]
        ]
    })
}

function sendChange(a, b) {
    var c = new ModifyAction(a, b, pageList.currPage);
    window.commInterface && commInterface.sendAction(c);
    1 < fabricCanvases.length && drawStack.redraw()
}

function initCanvasMain() {
    currentBrush = new InkBrush({
        width: 1,
        color: "#0000FF",
        transparency: 0,
        type: InkBrush.types.drawing
    });
    pageList = new PageList;
    pageList.addPageToEnd();
    pageList.setActivePage(1);
    undoStack = new UndoStack(pageList);
    commInterface = new WebSocketInterface;
    window.location.hash && commInterface.joinRoom(window.location.hash.substring(1));
    window.fontOptions = {
        fontSize: 16,
        fontWeight: "normal",
        fontFamily: "Helvetica",
        fontStyle: "normal",
        textDecoration: "none",
        color: "black"
    }
}

function clearCanvas() {
    var a = drawStack.clear();
    0 < a.length && (drawStack.redraw(), a = new RemoveAction(a, pageList.currPage, !0), undoStack.addAction(a), window.commInterface && commInterface.sendAction(a))
}

function undoDraw() {
    var a = undoStack.undo();
    a && window.commInterface && commInterface.sendAction(a)
}

function redoDraw() {
    var a = undoStack.redo();
    a && window.commInterface && commInterface.sendAction(a)
}

function setDim() {
    var a = fabricCanvas.getActiveObject(),
        b = !1,
        c = 0,
        d = 0,
        e = !1;
    a && (a.parentObject instanceof TextBox || a.parentObject instanceof MathQuillBox) && ("" == a.text ? (e = !0, c = a.parentObject.xpos, d = a.parentObject.ypos) : b = !0);
    fabricCanvas.setWidth(window.innerWidth);
    fabricCanvas.setHeight(window.innerHeight);
    drawStack.redraw();
    b && (a.parentObject.setActive(), fabricCanvas.getActiveObject().hiddenTextarea.focus());
    e && (fabricCanvas.isDrawingMode = !1, a = new TextBox({
            xpos: c,
            ypos: d,
            color: currentBrush.color
        }),
        drawStack.addToFront(a), drawStack.redraw(), a.setActive(), fabricCanvas.getActiveObject().hiddenTextarea.focus(), a = new AddAction([a], pageList.currPage), window.commInterface && commInterface.sendAction(a))
}

function updateViewportZoom(a) {
    drawStack.updateViewportZoom(a)
}

function setViewportZoom(a) {
    drawStack.updateViewportZoom(a, !0)
}

function setColor(a) {
    currentBrush.change({
        color: a
    });
    window.fontOptions.color = a
}

function setLineWidth(a) {
    a = parseInt(a);
    currentBrush.change({
        width: a
    })
}

function setTransparency(a) {
    a = parseFloat(a);
    currentBrush.change({
        transparency: a
    })
}

function setBrushTip(a) {
    currentBrush.change({
        tip: a
    })
}

function setFontOptions(a) {
    $.extend(window.fontOptions, a)
}

function setBrushType(a) {
    $.each(window.fabricCanvases, function(b, c) {
        var d = c.canvas,
            e = c.viewport;
        d.freeDrawingBrush.dispose && d.freeDrawingBrush.dispose();
        currentBrush.change({
            type: a
        });
        d.isDrawingMode = !0;
        switch (a) {
            case InkBrush.types.drawing:
                d.freeDrawingBrush = new DrawingBrush(e);
                break;
            case InkBrush.types.line:
                d.freeDrawingBrush = new LineBrush(e);
                break;
            case InkBrush.types.stroke_eraser:
                d.freeDrawingBrush = new StrokeEraserBrush(e);
                break;
            case InkBrush.types.selection:
                d.isDrawingMode = !1;
                d.freeDrawingBrush =
                    new SelectionBrush(e);
                break;
            case InkBrush.types.text:
                d.freeDrawingBrush = new TextBrush(e);
                break;
            case InkBrush.types.pointer:
                d.freeDrawingBrush = new PointerBrush(e);
                break;
            case InkBrush.types.math:
                d.freeDrawingBrush = new MathBrush(e);
                break;
            case InkBrush.types.move:
                d.freeDrawingBrush = new ViewportBrush(e)
        }
    })
}

function editFabricCanvasViewport(a, b) {
    $.each(window.fabricCanvases, function(c) {
        c.canvas == a && (c.viewport = b)
    })
};

function getCORSURL(a) {
    if (a = /\/\/(.*)/.exec(a))
        if (a = a[1]) return "https://bhavinder.gitlab.io/webrtc/whiteboard/white/corsproxy/" + a;
    return null
}
var PixelsPerUnit = {
    m: 3780,
    cm: 37.8,
    mm: 3.78,
    "in": 96,
    pt: 4 / 3,
    pc: 16,
    em: 16,
    ex: 8
};

function unitsToPixels(a, b, c) {
    var d = PixelsPerUnit[b];
    return null == d || "px" == b ? Math.ceil(a) : null != c ? Math.ceil(d / 96 * a * c) : null != this.dpi ? Math.ceil(d / 96 * a * this.dpi) : null == c ? (c = window.dpi, Math.ceil(d / 96 * a * c)) : a * d
}
genGUID = function() {
    function a() {
        return Math.floor(65536 * (1 + Math.random())).toString(16).substring(1)
    }
    return a() + a() + "-" + a() + "-" + a() + "-" + a() + "-" + a() + a() + a()
};

function genRoomID() {
    return genGUID().substring(0, 8)
}

function boxesIntersect(a, b) {
    function c(a, b) {
        for (var c = [], d = 0; 4 > d; d++) c.push(a[d].x * b.x + a[d].y * b.y);
        return {
            min: Math.min.apply(null, c),
            max: Math.max.apply(null, c)
        }
    }
    for (var d = [a, b], e = 0; 1 >= e; e++)
        for (var f = 0; 1 >= f; f++) {
            var g = {
                    x: d[e][f + 1].y - d[e][f].y,
                    y: d[e][f].x - d[e][f + 1].x
                },
                h = c(a, g),
                g = c(b, g);
            if (h.max < g.min || g.max < h.min) return !1
        }
    return !0
}

function degreesToRadians(a) {
    return Math.PI / 180 * a
}

function traceSegToRect(a, b, c, d, e, f) {
    d = d ? d : 1;
    e = e ? e : 1;
    var g = a.x * d;
    a = a.y * e;
    var h = b.x * d;
    b = b.y * e;
    c *= (d + e) / 2;
    f && (d = degreesToRadians(f), e = g * Math.cos(d) - a * Math.sin(d), a = g * Math.sin(d) + a * Math.cos(d), g = e, e = h * Math.cos(d) - b * Math.sin(d), b = h * Math.sin(d) + b * Math.cos(d), h = e);
    if (g == h && a == b) return [{
        x: g + c / 2,
        y: a + c / 2
    }, {
        x: g - c / 2,
        y: a + c / 2
    }, {
        x: g - c / 2,
        y: a - c / 2
    }, {
        x: g + c / 2,
        y: a - c / 2
    }];
    if (a == b) return [{
        x: g,
        y: a + c / 2
    }, {
        x: g,
        y: a - c / 2
    }, {
        x: h,
        y: b - c / 2
    }, {
        x: h,
        y: b + c / 2
    }];
    if (g == h) return [{
        x: g - c / 2,
        y: a
    }, {
        x: g + c / 2,
        y: a
    }, {
        x: h + c / 2,
        y: b
    }, {
        x: h - c / 2,
        y: b
    }];
    d = h -
        g;
    e = b - a;
    c = c / 2 / Math.sqrt(1 + Math.pow(d / e, 2));
    d = -d * c / e;
    return [{
        x: g + c,
        y: a + d
    }, {
        x: h + c,
        y: b + d
    }, {
        x: h - c,
        y: b - d
    }, {
        x: g - c,
        y: a - d
    }]
}
cloneObject = function(a) {
    var b = {},
        c;
    for (c in a) a.hasOwnProperty(c) && (b[c] = a[c]);
    return b
};

function dataURLToBuffer(a) {
    a = atob(a.split(",")[1]);
    for (var b = [], c = 0; c < a.length; c++) b.push(a.charCodeAt(c));
    return new Uint8Array(b)
}

function msieversion() {
    var a = window.navigator.userAgent,
        b = a.indexOf("MSIE ");
    return 0 < b || navigator.userAgent.match(/Trident.*rv\:11\./) ? parseInt(a.substring(b + 5, a.indexOf(".", b))) : -1
}

function isPhoneGap() {
    try {
        return (cordova || PhoneGap || phonegap) && /^file:\/{3}[^\/]/i.test(window.location.href) && /ios|iphone|ipod|ipad|android/i.test(navigator.userAgent)
    } catch (a) {}
    return !1
}

function isApp() {
    return -1 === document.URL.indexOf("http://") && -1 === document.URL.indexOf("https://") ? !0 : !1
}

function checkServerStatus(a, b) {
    var c = document.body.appendChild(document.createElement("script"));
    c.onload = function() {
        b(!0)
    };
    c.onerror = function() {
        b(!1)
    };
    c.src = a
}

function aspectIsVertical() {
    return !aspectIsHorizontal()
}

function aspectIsHorizontal() {
    return window.innerWidth >= window.innerHeight ? !0 : !1
}

function showMainCanvasCoordinates(a) {
    window.DEBUG && (window.canvasPositionDebug.style.visibility = a ? "visible" : "hidden")
}
(function() {
    if (window.DEBUG) {
        window.showingMainCanvasCoordinates = !0;
        var a = document.createElement("DIV");
        a.id = "canvasPositionNotifier";
        a.style.position = "fixed";
        a.style.zIndex = "100";
        a.style.top = "50px";
        a.style.left = "15px";
        a.style.visibility = "hidden";
        document.body.appendChild(a);
        window.canvasPositionDebug = a
    }
})();
ControlBar = function(a, b, c, d, e) {
    this.name = "";
    this.controlItems = [];
    this.height = this.width = 0;
    this.widthParent = !1;
    this.setupInputWidthAndHeight(c, d);
    this.dpi = 96;
    this.xpos = a;
    this.ypos = b;
//bhavinder
//    this.bgcolor = "rgb(235,235,235)";
    this.id = genGUID() + "-ControlBar";
    this.elem = null;
    this.position = "absolute";
    this.gravity = "top";
    this.toggleButtonB = this.toggleButtonA = null;
    this.toggleable = !1;
    this.toggleType = "slideToggle";
    this.isVisible = !1;
    this.groupWidth = 0;
    this.overrideWidth = this.overrideRows = !1;
    this.parent = null;
    this.freeWidth =
        1 == this.widthfit ? 0 : this.width;
    null != e && (this.toggleable = e);
    this.postDisplayCallbacks = []
};
ControlBar.prototype = {
    setupInputWidthAndHeight: function(a, b) {
        "fit" == a ? (this.widthfit = !0, this.width = 0) : ("window" == a ? (this.width = window.innerWidth, this.widthParent = !0) : this.width = a, this.widthfit = !1);
        "fit" == b ? (this.rowsfit = !0, this.rows = 1) : (this.rows = null != b ? b : 1, this.rowsfit = !1)
    },
    generateDiv: function(a) {
        this.elem = document.createElement("DIV");
        this.elem.id = this.id;
        this.elem.style.backgroundColor = this.bgcolor;
        this.elem.draggable = !1;
        this.elem.className += "unselectable";
        this.elem.setAttribute("style", this.elem.getAttribute("style") +
            "-moz-user-select: -moz-none; -webkit-user-select: none; -ms-user-select: none; user-select: none;");
        this.elem.style.position = this.position;
        this.groupWidth > window.innerWidth - this.xpos && !this.overrideRows && ($.each(this.controlItems, function() {
            this instanceof ControlGroup && !this.alwaysCollapsed && (this.isCollapsed = !0)
        }), this.calculateGroupWidth(), this.rows = Math.ceil(this.groupWidth / (window.innerWidth - this.xpos)), this.elem.style.width = window.innerWidth + "px");
        1 != this.widthfit || this.overrideWidth ? this.widthParent ?
            (this.elem.style.width = window.innerWidth + "px", this.width = window.innerWidth) : null != this.width ? this.elem.style.width = this.width + "px" : (console.log("auto..."), this.elem.style.width = "auto") : this.elem.style.width = "auto";
        this.height = unitsToPixels(1, "cm") * this.rows * window.menuSystemScaleFactor;
        this.elem.style.height = this.height + "px";
        this.rowsfit && (this.elem.style.height = "auto");
        var b = this;
        $.each(this.controlItems, function() {
            var a = this.generateElement();
            this instanceof ControlGroup && !this.isCollapsed && 0 < this.postDisplayCallbacks.length &&
                $.each(this.postDisplayCallbacks, function() {
                    b.postDisplayCallbacks.push(this)
                });
            if (a.element && a.callback && a.caller) {
                try {
                    b.elem.appendChild(a.element)
                } catch (d) {
                    console.log(d)
                }
                b.postDisplayCallbacks.push(a)
            } else b.elem.appendChild(a)
        });
        this.height + this.ypos > window.innerHeight && (this.ypos -= this.ypos - (this.height - window.defaultMenuSystemStyles.rowHeight));
        this.widthfit ? (b = this, this.postDisplayCallbacks.push({
            callback: function() {
                var a = document.getElementById(b.id);
                a.style.left = b.xpos + "px";
                var d = a.getBoundingClientRect(),
                    e = b.xpos;
                if (d.right == window.innerWidth)
                    for (; d.right == window.innerWidth && 1 <= e;) e--, a.style.left = e + "px", d = a.getBoundingClientRect(), 0 > e && (makeScroll = !0)
            },
            caller: this
        })) : this.elem.style.left = this.xpos + "px";
        "top" == this.gravity && (this.elem.style.top = this.ypos + "px");
        "bottom" == this.gravity && (this.elem.style.bottom = this.ypos + "px");
		
		
 //       "top" == this.gravity && (this.elem.style.top = this.ypos + "px");
  //      "bottom" == this.gravity && (this.elem.style.bottom = this.ypos + "px");
//		        this.elem.className += "unselectable";
		
		"top" == this.gravity && (this.elem.className = "qbswbtop " + this.elem.className);
		"bottom" == this.gravity && (this.elem.className = "qbswbbottom " + this.elem.className);
		
//        this.elem.style.zIndex = "100";
        $.each(this.elem.childNodes, function() {
			// Control Group Parent Div Css
//            this.style.cssFloat = "left";

			this.style.cssFloat = "left";
//			alert();
			if(this.parentNode.className.split(' ').indexOf('qbswbtop')>=0)
			{

		// Top ka Maal			
			this.style.cssFloat = "left";
			this.style.display = "block";
			this.style.clear = "both";
			this.style.height = "auto";
			}




//			alert(this.parentNode.className);
			
			
			
			



        });
		
//		"bottom" == this.gravity && (this.elem.style.backgroundColor = "rgb(255,0,0)");
//		"top" == this.gravity && (this.elem.style.display = "block");
//		"top" == this.gravity && (this.elem.style.clear = "both");
//		"top" == this.gravity && (this.elem.style.style.height = "auto");
		
        this.isVisible = !0;
        if (a) return this.elem
    },
    destroy: function() {
        try {
            var a = document.getElementById(this.id);
            this.divPos ? (this.divPos.removeChild(a), this.divPos = null) : BODY.removeChild(a);
            test = this;
            this.isVisible = !1
        } catch (b) {}
    },
    updateDiv: function() {
        this.widthParent && (this.width = window.innerWidth);
        this.calculateGroupWidth();
        this.destroy();
        this.initShow()
    },
    resizeDiv: function(a, b) {
        this.width = a;
        this.rows = b;
        this.updateDiv()
    },
    initShow: function(a) {
        this.generateDiv();
        a ? (a.appendChild(this.elem), this.divPos = a) : BODY.appendChild(this.elem);
        this.isVisible = !0;
        if (0 < this.postDisplayCallbacks.length) {
            var b = this;
            $.each(this.postDisplayCallbacks,
                function() {
                    this.callback(this.caller, b)
                });
            this.postDisplayCallbacks = []
        }
    },
    show: function() {
        switch (this.toggleType) {
            case "slideToggle":
                $("#" + this.id).slideToggle("fast");
                break;
            case "slide":
                $("#" + this.id).slideDown("fast");
                break;
            case "fade":
                $("#" + this.id).fadeIn("fast");
                break;
            case "hide":
                $("#" + this.id).show();
                break;
            default:
                $("#" + this.id).show().slideToggle("fast")
        }
        this.isVisible = !0;
        var a = this;
        setTimeout(function() {
            a.updateDiv()
        }, 250)
    },
    hide: function() {
        switch (this.toggleType) {
            case "slideToggle":
                $("#" + this.id).slideToggle("fast");
                break;
            case "slide":
                $("#" + this.id).slideUp("fast");
                break;
            case "fade":
                $("#" + this.id).fadeIn("fast");
                break;
            case "hide":
                $("#" + this.id).hide();
                break;
            default:
                $("#" + this.id).slideToggle("fast")
        }
        this.isVisible = !1;
        hideAllMenuControlBars()
    },
    toggle: function() {
        if (this.isVisible) {
            this.hide();
            var a = this.toggleButtonB.generateElement();
            a.element && a.callback && a.caller && (a.callback(), a = a.element);
            a.style.position = this.position;
            a.style.left = window.innerWidth - unitsToPixels(1.4, "cm") * window.menuSystemScaleFactor + "px";
            "top" == this.gravity && (a.style.top = this.ypos + "px");
            "bottom" == this.gravity && (a.style.bottom = this.ypos + "px");
            this.switchButton = a;
            BODY.appendChild(a);
            $("#" + this.toggleButtonB.id).toggle(0);
            $("#" + this.toggleButtonB.id).slideToggle("medium")
        } else this.show(), BODY.removeChild(this.switchButton)
    },
    setToggleButton: function(a, b) {
        var c = this;
        this.toggleButtonA = a;
        this.toggleButtonA.marginLeft = 0;
        this.toggleButtonA.marginRight = 0;
        this.toggleButtonA.width = unitsToPixels(1.2, "cm");
        this.toggleButtonB = b;
        this.toggleButtonB.borderLeft =
            0;
        this.toggleButtonB.borderRight = 0;
        this.toggleButtonB.borderTop = 0;
        this.toggleButtonB.borderBottom = 0;
        this.toggleButtonB.backgroundColor = "rgba(255,255,255,0)";
        this.toggleButtonA.callbackFn = function() {
            c.toggle()
        };
        this.toggleButtonB.callbackFn = function() {
            c.toggle()
        }
    },
    updateWidth: function() {
        this.widthParent && (this.width = window.innerWidth)
    },
    calculateGroupWidth: function() {
        var a = 0,
            b = this.freeWidth = 1 == this.widthfit ? 0 : this.width;
        $.each(this.controlItems, function() {
            this.updateWidth();
            if (null == this.marginLeft ||
                "undefined" == typeof this.marginLeft) this.marginLeft = 0;
            if (null == this.marginRight || "undefined" == typeof this.marginRight) this.marginRight = 0;
            if (null == this.width || "undefined" == typeof this.width) this.width = 0;
            if (null == this.borderLeft || "undefined" == typeof this.borderLeft) this.borderLeft = 0;
            if (null == this.borderRight || "undefined" == typeof this.borderRight) this.borderRight = 0;
            var c = this.width + this.marginLeft + this.marginRight + this.borderLeft + this.borderRight;
            a += Math.ceil(c);
            b -= Math.ceil(c)
        });
        this.groupWidth = a * window.menuSystemScaleFactor;
        this.freeWidth = 0 <= b ? b : 0
    },
    _newObjectWidthCompute: function(a) {
        a.float = "left";
        if (null == a.marginLeft || "undefined" == typeof a.marginLeft) a.marginLeft = 0;
        if (null == a.marginRight || "undefined" == typeof a.marginRight) a.marginRight = 0;
        if (null == a.width || "undefined" == typeof a.width) a.width = 0;
        var b = a.width + a.marginLeft + a.marginRight;
        this.groupWidth += b;
        this.freeWidth -= b;
        return a
    },
    addControlItem: function(a) {
        a = this._newObjectWidthCompute(a);
        this.controlItems.push(a)
    },
    addControlItemToFront: function(a) {
        a = this._newObjectWidthCompute(a);
        this.controlItems.unshift(a)
    },
    addControlItemAtIndex: function(a, b) {
        a = this._newObjectWidthCompute(a);
        this.controlItems.splice(b, 0, a)
    }
};
ControlItem = function() {};
ControlItem.prototype.generateElement = function() {};
ControlItem.prototype.updateWidth = function() {};
ControlGroup = function(a) {
    null == a && (a = []);
    this.selectionGroup = !1;
    this.controlItems = a;
    this.elem = null;
    this.id = genGUID() + "-ControlGroup";
    this.width = this.groupWidth = this.posxmod = 0;
    this.parent = null;
    this.isCollapsed = this.alwaysCollapsed = this.menuVisible = !1;
    this.isCollapsable = !0;
    this.toolTip = null;
    var b = this;
    this.collapsedButton = new ControlButton(function() {
        b.callbackFn(b)
    });
    this.showDividers = !0;
    this.leftDiv = document.createElement("DIV");
    this.rightDiv = document.createElement("DIV");
    this.leftDiv.style.backgroundColor =
        "rgb(170,170,170)";
    this.leftDiv.style.width = "1px";
    this.isInAMenu = !1;
    this.rightDiv = this.leftDiv;
    this.groupWidth += 2;
    this.width += 2;
    this.fn = null;
    this.postDisplayCallbacks = []
};
ControlGroup.prototype = {
    generateElement: function() {
        if ((this.isCollapsed || this.alwaysCollapsed) && this.isCollapsable) {
            this.collapsedButton.enabled = this.enabled ? !0 : !1;
            var a = this.collapsedButton.generateElement();
            this.elem = a.element && a.callback && a.caller ? a.element : a;
            this.elem.id = this.id;
            this.width = (this.collapsedButton.width + this.collapsedButton.marginLeft + this.collapsedButton.marginRight + this.collapsedButton.borderLeft + this.collapsedButton.borderRight + this.collapsedButton.paddingLeft + this.collapsedButton.paddingRight) *
                window.menuSystemScaleFactor;
            this.elem.draggable = !1;
            this.elem.className += "unselectable";
            this.elem.setAttribute("style", this.elem.getAttribute("style") + "-moz-user-select: -moz-none; -webkit-user-select: none; -ms-user-select: none; user-select: none;");
            this.elem.title = this.toolTip;
            return a.element && a.callback && a.caller ? {
                element: this.elem,
                callback: a.callback,
                caller: this
            } : this.elem
        }
        this.elem = document.createElement("DIV");
        var b = document.createElement("div");
        b.id = this.id;
        b.style.marginLeft = this.marginLeft;
        b.style.marginRight = this.marginRight;
        b.style.height = window.defaultMenuSystemStyles.rowHeight + "px";
        b.draggable = !1;
        b.className += "unselectable";
		
	
        var c = this;
        $.each(this.controlItems, function() {
            var a = this.generateElement();
            if (a.element && a.callback && a.caller) {
                try {
                    b.appendChild(a.element)
                } catch (e) {
                    console.log(e)
                }
                c.postDisplayCallbacks.push(a)
            } else b.appendChild(a)
        });
		
		
		
		
        $.each(b.childNodes, function() {
			// bhavinder button CSS
//alert(b.parentNode.id);

			
			
//top
			this.style.cssFloat = "left"; 
//			this.style.display = "block";

			
//bottom	
//			this.style.cssFloat = "none";
//			this.style.display = "block";

			
// bottom maal
			
//			alert(this.parent());
			
// top maal			
//            this.style.cssFloat = "none";
//			this.style.display = "block";
        });
		
		
		
		
		
		
//		"top" == this.gravity && (this.elem.style.cssFloat = "none");
//		"bottom" == this.gravity && (this.elem.style.cssFloat = "left");

		
        this.width = this.groupWidth;
        b.style.width = this.width * window.menuSystemScaleFactor +
            "px";
			// Control Group Div CSS
        b.style.cssFloat = "none";
//        this.leftDiv.style.height = unitsToPixels(.7, "cm") * window.menuSystemScaleFactor + "px";
//        this.leftDiv.style.marginTop = unitsToPixels(1.5, "mm") * window.menuSystemScaleFactor + "px";
//        this.leftDiv.style.marginBottom = unitsToPixels(1.5, "mm") * window.menuSystemScaleFactor + "px";
		// no impact
        this.leftDiv.style.cssFloat = "left";
        this.elem.appendChild(b);
        this.showDividers && b.appendChild(this.leftDiv);
        this.elem.style.height = window.defaultMenuSystemStyles.rowHeight + "px";
        return this.elem
    },
    alignFn: function(a,
        b) {
        var c = a.elem.getBoundingClientRect(),
            d = b.elem.getBoundingClientRect(),
            c = c.y - d.y;
        console.log(a.elem.getBoundingClientRect(), b.elem.getBoundingClientRect(), c);
        0 != c && (d = document.getElementById(a.id), d.style.top = "-" + c + "px", d.style.position = "relative")
    },
    _newObjectWidthCompute: function(a) {
        a.marginLeft = unitsToPixels(.5, "mm");
        a.marginRight = unitsToPixels(.5, "mm");
        var b = a.width;
        a.marginLeft && (b += a.marginLeft);
        a.marginRight && (b += a.marginRight);
        a.paddingLeft && (b += a.paddingLeft);
        a.paddingRight && (b += a.paddingRight);
        this.groupWidth += b;
        this.isCollapsed || this.alwaysCollapsed || (this.width = this.groupWidth);
        this.selectionGroup && (a.inSelectionGroup = !0, a.selectionGroup = this, a.setCallBackFn(a.callbackFn));
        return a
    },
    addControlItem: function(a) {
        a = this._newObjectWidthCompute(a);
        this.controlItems.push(a)
    },
    addControlItemToFront: function(a) {
        a = this._newObjectWidthCompute(a);
        this.controlItems.unshift(a)
    },
    addControlItemAtIndex: function(a, b) {
        a = this._newObjectWidthCompute(a);
        this.controlItems.splice(b, 0, a)
    },
    setSelected: function(a) {
        $.each(this.controlItems,
            function() {
                this.selected = this == a ? !0 : !1
            });
        (this.isCollapsed || this.alwaysCollapsed) && this.isCollapsable || this.updateDiv()
    },
    regenCallBacks: function() {},
    callbackFn: function(a) {
        this.menuVisible ? (hideAllMenuControlBars(), this.menuVisible = !1) : this.menuVisible || (hideAllMenuControlBars(), a = a.generateMenu(), window.activeMenuControlBars.push(a), a.initShow(), this.menuVisible = !0)
    },
    updateDiv: function() {
        newelem = this.generateElement();
        if (elem = document.getElementById(this.id)) elem.outerHTML = newelem.outerHTML, elem.innerHTML =
            newelem.innerHTML;
        this.regenCallBacks()
    },
    regenCallBacks: function() {
        $.each(this.controlItems, function() {
            this instanceof ControlButton && $("#" + this.id).click(this.callbackFn)
        })
    },
    generateMenu: function() {
        var a = this.collapsedButton.elem,
            b = a.getBoundingClientRect(),
            a = a.offsetLeft - a.scrollLeft - (unitsToPixels(.5, "mm") + this.posxmod * unitsToPixels(1.3, "cm")),
            c = 0,
            c = b.top + window.defaultMenuSystemStyles.rowHeight * window.menuSystemScaleFactor > window.innerHeight - 10 ? b.top - window.defaultMenuSystemStyles.rowHeight * window.menuSystemScaleFactor :
            b.top + window.defaultMenuSystemStyles.rowHeight * window.menuSystemScaleFactor;
        a + this.groupWidth > window.innerWidth && (a = window.innerWidth - this.groupWidth, 0 > a && (a = 0));
        var d = new ControlBar(a, c, "fit", "fit");
        This = this;
        d.parent = this;
        $.each(this.controlItems, function() {
            d.addControlItem(this)
        });
        return d
    },
    updateWidth: function() {
        this.width = (this.isCollapsed || this.alwaysCollapsed) && this.isCollapsable ? this.collapsedButton.width + this.collapsedButton.marginLeft + this.collapsedButton.marginRight + this.collapsedButton.borderLeft +
            this.collapsedButton.borderRight : this.groupWidth
    },
    addIcon: function(a) {
        this.icon = document.createElement("IMG");
        this.icon.src = a;
        a = unitsToPixels(1.5, "cm") - unitsToPixels(2, "mm") - unitsToPixels(5, "mm");
        this.icon.style.width = a + "px";
        this.icon.width = a;
        var b = unitsToPixels(1, "cm") - unitsToPixels(2, "mm");
        this.icon.style.height = b + "px";
        this.icon.height = b;
        this.icon.draggable = !1;
        this.icon.className += "unselectable";
        this.icon.setAttribute("style", this.icon.getAttribute("style") + "-moz-user-select: -moz-none; -webkit-user-select: none; -ms-user-select: none; user-select: none;");
        this.icon.style.border = "none";
        this.width = this.collapsedButton.width + this.collapsedButton.marginLeft + this.collapsedButton.marginRight;
        this.collapsedButton.content = this.icon;
        this.collapsedButton.contentWidth = a;
        this.collapsedButton.contentHeight = b
    }
};
ControlButton = function(a, b) {
    this.acontent = this.acArgs = this.contentHeight = this.contentWidth = this.content = null;
    this.dpi = 96;
    this.id = genGUID() + "-ControlButton";
    this.inSelectionGroup = this.selected = !1;
    this.parent = this.selectionGroup = null;
    this.width = unitsToPixels(1, "cm");
    this.height = unitsToPixels(1, "cm");
    this.paddingTop = unitsToPixels(0, "mm");
    this.paddingRight = unitsToPixels(0, "mm");
    this.paddingLeft = unitsToPixels(0, "mm");
    this.paddingBottom = unitsToPixels(0, "mm");
    this.marginTop = unitsToPixels(0, "mm");
    this.marginBottom =
        unitsToPixels(0, "mm");
    this.marginLeft = unitsToPixels(.5, "mm");
    this.marginRight = unitsToPixels(.5, "mm");
    this.outline = "none";
    this.borderTop = unitsToPixels(1, "mm");
    this.borderRight = unitsToPixels(1, "mm");
    this.borderLeft = unitsToPixels(1, "mm");
    this.borderBottom = unitsToPixels(1, "mm");
    this.borderColor = "rgba(0,0,0,0)";
    this.backgroundColor = "rgb(235,235,235)";
    this.backgroundImage = "";
    this.float = "none";
	this.display = "block";
    this.isEnabled = this.enabled = !0;
    this.toolTip = null;
    this.baseCallback = a;
    this.cbArgs = b;
    this.callbackFn = this.prepCallBack(a)
};
ControlButton.prototype = {
    prepCallBack: function(a) {
        var b = this,
            c = this.selectionGroup,
            d = this.inSelectionGroup;
        return function() {
            1 == d && c.setSelected(b);
            a && a(this.cbArgs)
        }
    },
    setSelected: function(a) {
        1 == a && this.parent.setSelected(this);
        0 == a && (this.selected = !1, this.updateDiv())
    },
    updateDiv: function() {
        for (var a = this.parent; a && !(a instanceof ControlBar);) a = a.parent;
        a instanceof ControlBar && a.updateDiv()
    },
    setCallBackFn: function(a) {
        this.callbackFn = this.prepCallBack(a)
    },
    resetCallBackFn: function() {
        $(this.elem).click(this.callbackFn)
    },
    generateElement: function() {
        this.elem = document.createElement("Button");
        this.elem.id = this.id;
        this.elem.type = "button";
        this.elem.style.borderColor = this.borderColor;
        this.elem.style.borderTop = this.borderTop * window.menuSystemScaleFactor + "px solid " + window.defaultMenuSystemStyles.menuBackgroundColor;
        this.elem.style.borderRight = this.borderRight * window.menuSystemScaleFactor + "px solid " + window.defaultMenuSystemStyles.menuBackgroundColor;
        this.elem.style.borderLeft = this.borderLeft * window.menuSystemScaleFactor +
            "px solid " + window.defaultMenuSystemStyles.menuBackgroundColor;
        this.elem.style.borderBottom = this.borderBottom * window.menuSystemScaleFactor + "px solid " + window.defaultMenuSystemStyles.menuBackgroundColor;
        this.elem.style.backgroundColor = this.backgroundColor;
        this.elem.style.outline = this.outline * window.menuSystemScaleFactor;
        this.elem.style.marginLeft = this.marginLeft * window.menuSystemScaleFactor + "px";
        this.elem.style.marginRight = this.marginRight * window.menuSystemScaleFactor + "px";
        this.elem.style.marginTop =
            this.marginTop * window.menuSystemScaleFactor + "px";
        this.elem.style.marginBottom = this.marginBottom * window.menuSystemScaleFactor + "px";
        this.elem.style.paddingTop = this.paddingTop * window.menuSystemScaleFactor + "px";
        this.elem.style.paddingRight = this.paddingRight * window.menuSystemScaleFactor + "px";
        this.elem.style.paddingLeft = this.paddingLeft * window.menuSystemScaleFactor + "px";
        this.elem.style.paddingBottom = this.paddingBottom * window.menuSystemScaleFactor + "px";
        this.elem.style.width = this.width * window.menuSystemScaleFactor +
            "px";
        this.elem.style.height = this.height * window.menuSystemScaleFactor + "px";
        this.elem.style.backgroundImage = this.backgroundImage;
        this.elem.draggable = !1;
        this.elem.className += "unselectable";
        this.elem.setAttribute("style", this.elem.getAttribute("style") + "-moz-user-select: -moz-none; -webkit-user-select: none; -ms-user-select: none; user-select: none;");
        this.toolTip && (this.elem.title = this.toolTip);
        var a = !0;
        null != this.acontent && (ret = this.acontent(this.acArgs, this.elem, this.content, this.selected, this), this.elem =
            ret[0], this.content = ret[1], 2 < ret.length && (0 != ret[2] ? this.selected = ret[2] : a = !1));
        a && this.selected && (this.elem.style.borderColor = "rgba(0,0,0,0)", this.elem.style.borderTop = unitsToPixels(1, "mm") * window.menuSystemScaleFactor + "px solid " + window.defaultMenuSystemStyles.menuBackgroundColor, this.elem.style.borderRight = unitsToPixels(1, "mm") * window.menuSystemScaleFactor + "px solid " + window.defaultMenuSystemStyles.menuBackgroundColor, this.elem.style.borderLeft = unitsToPixels(1, "mm") * window.menuSystemScaleFactor +
            "px solid " + window.defaultMenuSystemStyles.menuBackgroundColor, this.elem.style.borderBottom = unitsToPixels(1, "mm") * window.menuSystemScaleFactor + "px solid " + window.defaultMenuSystemStyles.menuForgroundColor);
        null != this.content && (null != this.contentWidth && null != this.contentHeight && (this.icon && this.setIconDimensions(), this.content.style.width = this.contentWidth * window.menuSystemScaleFactor + "px", this.content.style.height = this.contentHeight * window.menuSystemScaleFactor + "px"), this.elem.appendChild(this.content));
        if (this.enabled) return this.callbackFn ? (b = this, {
            element: this.elem,
            callback: function() {
                $(b.elem).unbind();
                $(b.elem).off();
                $(b.elem).on("click", b.callbackFn)
            },
            caller: this
        }) : this.elem;
        this.elem.style.backgroundColor = window.defaultMenuSystemStyles.menuDisabledColor;
        var b = this;
        return {
            element: this.elem,
            callback: function() {
                $(b.elem).unbind();
                $(b.elem).off()
            },
            caller: this
        }
    },
    updateWidth: function() {},
    addContent: function(a) {
        this.content = a
    },
    addActiveContent: function(a) {
        this.acontent = a
    },
    addIcon: function(a) {
        this.icon =
            document.createElement("IMG");
        this.icon.src = a;
        this.setIconDimensions()
    },
    setIconDimensions: function() {
        var a = this.width - unitsToPixels(0, "mm") - unitsToPixels(5, "mm");
        this.icon.style.width = a + "px";
        var b = this.height - unitsToPixels(2, "mm") - unitsToPixels(2.8, "mm");
        this.icon.style.height = b + "px";
        this.icon.draggable = !1;
        this.icon.className += "unselectable";
        this.icon.setAttribute("style", this.icon.getAttribute("style") + "-moz-user-select: -moz-none; -webkit-user-select: none; -ms-user-select: none; user-select: none;");
        this.icon.style.border = "none";
        this.content = this.icon;
        this.contentWidth = a;
        this.contentHeight = b
    }
};
ControlTextField = function(a, b, c, d, e) {
    this.id = genGUID() + "-ControlTextField";
    this.width = 0;
    this.width = a ? a : window.defaultMenuSystemStyles.buttonWidth;
    this.height = window.defaultMenuSystemStyles.rowHeight;
    this.parent = e;
    this.text = b;
    this.fontSize = c ? c : 12;
    this.paddingLeft = unitsToPixels(1, "mm");
    this.paddingRight = unitsToPixels(1, "mm");
    this.postDisplayCallback = null
};
ControlTextField.prototype = {
    updateWidth: function() {},
    generateElement: function() {
        this.elem = document.createElement("DIV");
        this.elem.id = this.id;
        this.elem.width = this.width * window.menuSystemScaleFactor;
        this.elem.style.width = this.elem.width + "px";
        this.elem.height = (window.defaultMenuSystemStyles.rowHeight - this.fontSize) * window.menuSystemScaleFactor;
        this.elem.style.height = this.elem.height + "px";
        this.elem.style.paddingLeft = this.paddingLeft * window.menuSystemScaleFactor + "px";
        this.elem.style.paddingRight = this.paddingRight *
            window.menuSystemScaleFactor + "px";
        this.elem.style.marginTop = this.fontSize * window.menuSystemScaleFactor + "px";
        this.elem.style.fontSize = this.fontSize * window.menuSystemScaleFactor + "px";
        this.elem.style.textAlign = "center";
        this.elem.innerHTML = this.text;
        this.elem.draggable = !1;
        this.elem.className += "unselectable qbstext";
        this.elem.setAttribute("style", this.elem.getAttribute("style") + "-moz-user-select: -moz-none; -webkit-user-select: none; -ms-user-select: none; user-select: none;");
        return this.postDisplayCallback ? {
            element: this.elem,
            callback: this.postDisplayCallback,
            caller: this
        } : this.elem
    }
};
ControlTextBox = function(a, b, c, d, e, f) {
    this.id = genGUID() + "-ControlTextBox";
    this.width = a ? a : window.defaultMenuSystemStyles.buttonWidth;
    this.height = window.defaultMenuSystemStyles.rowHeight;
    this.parent = f;
    this.text = b;
    this.fontSize = c ? c : 12;
    this.paddingLeft = unitsToPixels(1, "mm");
    this.paddingRight = unitsToPixels(1, "mm");
    this.marginLeft = unitsToPixels(1, "mm");
    this.marginRight = unitsToPixels(1, "mm");
    this.callBackFn = e;
    this.postDisplayCallback = null
};
ControlTextBox.prototype = {
    compositeCallbackFns: function(a, b) {
        var c = this;
        return function() {
            a && a(c);
            b && b(c)
        }
    },
    updateWidth: function() {},
    generateElement: function() {
        this.elem = document.createElement("input");
        this.elem.type = "text";
        this.elem.id = this.id;
        this.elem.style.width = this.width * window.menuSystemScaleFactor + "px";
        this.elem.style.height = (window.defaultMenuSystemStyles.rowHeight - this.fontSize) * window.menuSystemScaleFactor + "px";
        this.elem.style.paddingLeft = this.paddingLeft * window.menuSystemScaleFactor + "px";
        this.elem.style.paddingRight = this.paddingRight * window.menuSystemScaleFactor + "px";
        this.elem.style.marginTop = this.fontSize / 2.5 * window.menuSystemScaleFactor + "px";
        this.elem.style.marginLeft = this.marginLeft * window.menuSystemScaleFactor + "px";
        this.elem.style.marginRight = this.marginRight * window.menuSystemScaleFactor + "px";
        this.elem.style.border = "0px";
        this.elem.style.fontSize = this.fontSize * window.menuSystemScaleFactor + "px";
        this.elem.style.textAlign = "center";
        this.elem.value = this.text;
        this.elem.draggable = !1;
        this.elem.className += "unselectable";
        this.elem.setAttribute("style", this.elem.getAttribute("style") + "-moz-user-select: -moz-none; -webkit-user-select: none; -ms-user-select: none; user-select: none;");
        return {
            element: this.elem,
            callback: this.compositeCallbackFns(this.callBackFn, this.postDisplayCallback),
            caller: this
        }
    }
};
ControlDropDownMenu = function(a, b, c, d, e, f) {
    this.id = genGUID() + "-ControlTextBox";
    this.width = null;
    this.width = a ? a : window.defaultMenuSystemStyles.buttonWidth;
    this.height = window.defaultMenuSystemStyles.rowHeight;
    this.parent = f;
    this.list = b;
    this.fontSize = c ? c : 12;
    this.paddingLeft = unitsToPixels(1, "mm");
    this.paddingRight = unitsToPixels(1, "mm");
    this.marginLeft = unitsToPixels(1, "mm");
    this.marginRight = unitsToPixels(1, "mm");
    this.callBackFn = e;
    this.postDisplayCallback = null
};
ControlDropDownMenu.prototype = {
    compositeCallbackFns: function(a, b) {
        var c = this;
        return function() {
            if (c.callBackFn) $(c.elem).on("change", function() {
                this.selectedIndex && c.callBackFn(this, c)
            });
            a && a();
            b && b(c)
        }
    },
    updateWidth: function() {},
    generateElement: function() {
        this.elem = document.createElement("select");
        this.elem.id = this.id;
        this.elem.style.width = this.width * window.menuSystemScaleFactor + "px";
        this.elem.style.height = (window.defaultMenuSystemStyles.rowHeight - this.fontSize) * window.menuSystemScaleFactor + "px";
        this.elem.style.paddingLeft = this.paddingLeft * window.menuSystemScaleFactor + "px";
        this.elem.style.paddingRight = this.paddingRight * window.menuSystemScaleFactor + "px";
        this.elem.style.marginTop = this.fontSize / 2.5 * window.menuSystemScaleFactor + "px";
        this.elem.style.marginLeft = this.marginLeft * window.menuSystemScaleFactor + "px";
        this.elem.style.marginRight = this.marginRight * window.menuSystemScaleFactor + "px";
        this.elem.style.border = "0px";
        this.elem.style.fontSize = this.fontSize * window.menuSystemScaleFactor + "px";
        this.elem.draggable = !1;
        this.elem.className += "unselectable";
        this.elem.setAttribute("style", this.elem.getAttribute("style") + "-moz-user-select: -moz-none; -webkit-user-select: none; -ms-user-select: none; user-select: none;");
        var a = this;
        $.each(this.list, function() {
            var b = document.createElement("option");
            b.value = this.value;
            b.innerHTML = this.value;
            a.elem.appendChild(b)
        });
        return {
            element: this.elem,
            callback: this.compositeCallbackFns(this.callBackFn, this.postDisplayCallback),
            caller: this
        }
    }
};
ControlSpacer = function(a, b, c) {
    this.id = genGUID() + "-ControlSpacer";
    this.fillParent = !1;
    this.parent = null;
    b ? (this.fillParent = b, this.parent = c) : (this.fillParent = !1, this.parent = c ? c : null);
    this.width = a
};
ControlSpacer.prototype = {
    updateWidth: function() {},
    generateElement: function() {
        if (this.fillParent) return this.elem = document.createElement("DIV"), this.elem.id = this.id, this.parent.calculateGroupWidth(), this.elem.style.height = "1px", this.elem.draggable = !1, this.elem.className += "unselectable", this.elem.setAttribute("style", this.elem.getAttribute("style") + "-moz-user-select: -moz-none; -webkit-user-select: none; -ms-user-select: none; user-select: none;"), {
            element: this.elem,
            callback: this.setFinalWidth,
            caller: this
        };
        this.elem = document.createElement("DIV");
        this.elem.id = this.id;
        this.elem.style.width = this.width * window.menuSystemScaleFactor + "px";
        this.elem.style.height = "1px";
        this.elem.draggable = !1;
        this.elem.className += "unselectable";
        this.elem.setAttribute("style", this.elem.getAttribute("style") + "-moz-user-select: -moz-none; -webkit-user-select: none; -ms-user-select: none; user-select: none;");
        return this.elem
    },
    setFinalWidth: function(a, b) {
        var c = document.getElementById(b.controlItems[b.controlItems.length - 1].id),
            c =
            window.innerWidth - (c.offsetLeft + c.offsetWidth + b.controlItems[b.controlItems.length - 1].marginRight) - 1;
        0 > c && (c = 0);
        document.getElementById(a.id).style.width = c + "px"
    }
};

function initControls(a) {
    window.defaultMenuSystemStyles = {};
    window.defaultMenuSystemStyles.rowHeight = unitsToPixels(1, "cm");
    window.defaultMenuSystemStyles.buttonWidth = unitsToPixels(1.2, "cm");
    window.defaultMenuSystemStyles.menuBackgroundColor = "rgb(235,235,235)";
    window.defaultMenuSystemStyles.menuForgroundColor = "rgb(102,102,102)";
    window.defaultMenuSystemStyles.menuDisabledColor = "rgb(225,225,225)";
    window.defaultMenuSystemStyles.menuDividerColor = "rgb(170,170,170)";
    window.menuSystemScaleFactor = 1;
    window.allActiveButtons = [];
    window.allWindowControlBars = [];
    window.activeMenuControlBars = [];
    window.allControlGroups = {};
    window.allControlButtons = {};
    window.allControlBars = {};
    window.allControlTextFields = {};
    window.allControlTextBoxes = {};
    window.allControlDropDownMenus = {};
    window.allToolControlButtons = [];
    window.allNavControlButtons = [];
    window.statusBar = new ControlBar(0, 0, "window", "fit");
    window.statusBar.position = "fixed";
    window.statusBar.gravity = "bottom";
    loadjsurl("menus/main_menu/layout.js");
    loadjsurl("menus/status_bar/layout.js");
    document.getElementById("main_menu_layout") && document.getElementById("status_bar_layout") ? (loadXMLfromID("main_menu_layout", null, 0, 0), loadXMLfromID("status_bar_layout", window.statusBar, 0, 0), a()) : (loadXMLfromUrl("menus/main_menu/layout.xml", null, 0, 0, function(a) {
        $.each(window.postMainControlBarLoad, function(c, d) {
            d(a)
        })
    }), loadXMLfromUrl("menus/status_bar/layout.xml", window.statusBar, 0, 0, function(a) {
        $.each(window.postStatusControlBarLoad, function(c, d) {
            d(a)
        })
    }));
    $(window).resize(function() {
        $.each(allWindowControlBars,
            function() {
                $.each(this.controlItems, function() {
                    this instanceof ControlGroup && (this.isCollapsed = !1)
                });
                this.destroy()
            });
        $.each(activeMenuControlBars, function() {
            this.destroy()
        });
        window.allActiveButtons = [];
        window.activeMenuControlBars = [];
        window.mainControlBar.rowsfit = !0;
        window.statusBar.rowsfit = !0;
        window.mainControlBar.updateDiv();
        window.statusBar.updateDiv();
        setDim()
    })
}

function loadXMLfromID(a, b, c, d, e) {
    a = document.getElementById(a).innerHTML;
    $($.parseXML(a)).children("menuBar").each(function() {
        inflateXMLMenuLayout($(this), b, c, d, e)
    })
}

function loadXMLfromUrl(a, b, c, d, e) {
    var f = new XMLHttpRequest;
    f.onreadystatechange = function() {
        4 != f.readyState || 200 != f.status && 0 != f.status || (a = $.parseXML(f.responseText), $xml = $(a), $xml.children("menuBar").each(function() {
            inflateXMLMenuLayout($(this), b, c, d, e)
        }))
    };
    f.open("GET", a, !0);
    f.send()
}

function loadjsurl(a) {
    var b = document.createElement("script");
    b.setAttribute("type", "text/javascript");
    b.setAttribute("src", a);
    BODY.appendChild(b)
}

function inflateXMLMainMenuLayout(a) {
    inflateXMLMenuLayout(a, window.mainControlBar)
}

function inflateXMLMenuLayout(a, b, c, d, e) {
    var f = 0,
        g = 1;
    a.attr("width") && (f = a.attr("width"));
    a.attr("rows") && (g = a.attr("rows"));
    "main" == a.attr("type") && (b = new ControlBar(0, 0, "window", "fit"), b.gravity = "top", b.position = "fixed", window.mainControlBar = b);
    var h = !1;
    b || (b = new ControlBar(0, 0, "fit", "fit"), h = !0);
    h && (b.widthfit = !0);
    b.setupInputWidthAndHeight(f, g);
    a.attr("type");
    a.attr("rows") && (b.rows = a.attr("rows"));
    "true" == a.attr("overrideRows") && (b.overrideRows = !0);
    "true" == a.attr("overrideWidth") && (b.overrideWidth = !0);
    a.children("content").each(function() {
        inflateXMLContentLayout($(this), b)
    });
    a.children("keyBindings").each(function() {
        inflateXMLKeyBindings($(this))
    });
    a.attr("name") && (b.name = a.attr("name"), window.allControlBars[b.name] = b);
    c && (b.xpos = c);
    d && (b.ypos = d);
    "true" == a.attr("show") && (b.initShow(), "true" != a.attr("destroyable") && "main" != a.attr("type") && window.activeMenuControlBars.push(b));
    "true" == a.attr("destroyable") && window.activeMenuControlBars.push(b);
    "false" == a.attr("destroyable") && (window.activeMenuControlBars =
        window.activeMenuControlBars.filter(function(a) {
            return a != b
        }));
    window.allWindowControlBars.push(b);
    e && e(b);
    return window.allWindowControlBars.length - 1
}

function inflateXMLKeyBindings(a) {
    a.children().each(function() {
        if ("bind" == $(this).context.nodeName) {
            var a = $(this).attr("keyCode"),
                c = window[$(this).attr("function")];
            $(document).bind("keydown", a, function() {
                c()
            })
        }
    })
}

function inflateXMLContentLayout(a, b) {
    a.children().each(function() {
        "controlGroup" == $(this).context.nodeName && inflateXMLGroupLayout($(this), b);
        "spacer" == $(this).context.nodeName && (spacer = "fillParent" == $(this).attr("width") ? new ControlSpacer(0, !0, b) : new ControlSpacer($(this).attr("width"), !1, b), b.addControlItem(spacer), spacer.parent = b);
        "button" == $(this).context.nodeName && inflateXMLButtonLayout($(this), b);
        "textField" == $(this).context.nodeName && inflateXMLTextFieldLayout($(this), b);
        "textBox" == $(this).context.nodeName &&
            inflateXMLTextBoxLayout($(this), b);
        "dropDownMenu" == $(this).context.nodeName && inflateXMLDropDownMenuLayout($(this), b)
    })
}

function inflateXMLTextFieldLayout(a, b) {
    var c = window.defaultMenuSystemStyles.buttonWidth,
        d = null,
        e = null,
        f = null,
        g = null;
    a.attr("width") && (c = parseInt(a.attr("width")));
    a.attr("text") && (d = a.attr("text"));
    a.attr("fontSize") && (e = a.attr("fontSize"));
    a.attr("font") && (f = a.attr("font"));
    a.attr("postDisplayCallback") && (g = window[a.attr("postDisplayCallback")]);
    c = new ControlTextField(c, d, e, f, b);
    c.postDisplayCallback = g;
    a.attr("name") && (c.name = a.attr("name"), window.allControlTextFields[c.name] = c);
    b.addControlItem(c)
}

function inflateXMLTextBoxLayout(a, b) {
    var c = window.defaultMenuSystemStyles.buttonWidth,
        d = null,
        e = null,
        f = null,
        g = null,
        h = null;
    a.attr("width") && (c = parseInt(a.attr("width")));
    a.attr("text") && (d = a.attr("text"));
    a.attr("fontSize") && (e = a.attr("fontSize"));
    a.attr("font") && (f = a.attr("font"));
    a.attr("function") && (g = window[a.attr("function")]);
    a.attr("postDisplayCallback") && (h = window[a.attr("postDisplayCallback")]);
    c = new ControlTextBox(c, d, e, f, g, b);
    c.postDisplayCallback = h;
    a.attr("name") && (c.name = a.attr("name"),
        window.allControlTextBoxes[c.name] = c);
    b.addControlItem(c)
}

function inflateXMLDropDownMenuLayout(a, b) {
    var c = window.defaultMenuSystemStyles.buttonWidth,
        d = [],
        e = null,
        f = null,
        g = null,
        h = null;
    a.children("selection").each(function() {
        var a = $(this),
            b = a.attr("value"),
            a = a.attr("argument");
        d.push({
            value: b,
            args: a
        })
    });
    a.attr("width") && (c = parseInt(a.attr("width")));
    a.attr("fontSize") && (e = a.attr("fontSize"));
    a.attr("font") && (f = a.attr("font"));
    a.attr("selectionFunction") && (g = window[a.attr("selectionFunction")]);
    a.attr("postDisplayCallback") && (h = window[a.attr("postDisplayCallback")]);
    c = new ControlDropDownMenu(c, d, e, f, g, b);
    c.postDisplayCallback = h;
    a.attr("name") && (c.name = a.attr("name"), window.allControlDropDownMenus[c.name] = c);
    b.addControlItem(c)
}

function inflateXMLGroupLayout(a, b) {
    var c = new ControlGroup,
        d = !0;
    "false" == a.attr("enabled") && (d = !1);
    a.attr("name") && (c.name = a.attr("name"), window.allControlGroups[c.name] = c);
    a.attr("icon") && c.addIcon(a.attr("icon"));
    "true" == a.attr("selectionGroup") && (c.selectionGroup = !0);
    "false" == a.attr("collapsable") && (c.isCollapsed = !1, c.isCollapsable = !1);
    "true" == a.attr("collapsed") && (c.alwaysCollapsed = !0);
    a.attr("toolTip") && (c.toolTip = a.attr("toolTip"));
    a.children("content").each(function() {
        inflateXMLContentLayout($(this),
            c)
    });
    if (a.attr("function"))
        if (d) {
            var e = c.collapsedButton.callbackFn;
            c.collapsedButton.callbackFn = function() {
                e();
                window[a.attr("function")]()
            }
        } else c.collapsedButton.callbackFn = function() {};
    b.addControlItem(c);
    c.parent = b;
    c.enabled = d;
    if (a.attr("selectionFunction")) window[a.attr("selectionFunction")]();
    "false" == a.attr("showDividers") && (c.showDividers = !1);
    "true" == a.attr("test") && console.log("Group Gen", c, b)
}

function inflateXMLButtonLayout(a, b) {
    var c = null,
        d = "true",
        e = !1,
        f = !0,
        g = !1,
        h = !1,
        l = !1,
        m = !1;
    a.attr("hideOtherMenus") && (d = a.attr("hideOtherMenus"));
    a.attr("hideOtherSubMenus") && (e = a.attr("hideOtherSubMenus"));
    var n = "true";
    a.attr("collapsedHide") && (n = a.attr("collapsedHide"));
    "false" == a.attr("enabled") && (f = !1);
    "true" == a.attr("appDisabled") && isApp() && (f = !1);
    "true" == a.attr("toggleSelect") && (m = !0);
    "true" == a.attr("navType") && (g = !0);
    "true" == a.attr("toolType") && (h = !0);
    "true" == a.attr("clickSelect") && (l = !0);
    if (f) {
        var p;
        a.attr("args") && (p = a.attr("args"));
        var q = "";
        a.attr("function") && (q = a.attr("function"), c = "false" != d ? function() {
            hideAllMenuControlBars();
            (0, window[a.attr("function")])(p)
        } : function() {
            window[a.attr("function")](p)
        })
    }
    var k, r = function() {
        g && unselectTools();
        h && unselectNavs();
        l && (k.selected = !0, k.updateDiv());
        m && (k.selected = !k.selected, k.updateDiv())
    };
    k = new ControlButton(function() {
        f && (r(), c())
    });
    k.enabled = f;
    k.selected = !1;
    a.attr("name") && (k.name = a.attr("name"), window.allControlButtons[k.name] = k);
    window.allActiveButtons.push(k);
    f && a.children("menuBar").each(function() {
        var a = $(this),
            d = new ControlBar(0, 0, "fit", "fit");
        inflateXMLMenuLayout(a, d);
        compositeCallBackFn = function() {
            "true" == e && hideOtherSubMenus(b);
            var f = !1;
            $.each(window.activeMenuControlBars, function() {
                this.name == a.attr("name") && (f = !0)
            });
            if (f) hideAllMenuControlBars();
            else {
                var g, h;
                b instanceof ControlGroup ? 1 == b.isCollapsed ? (g = posx(k.parent.id, k, d), h = posy(k.parent.id, k, d)) : (g = posx(k.id, k, d), h = posy(k.id, k, d)) : (g = posx(k.id, k, d), h = posy(k.id, k, d));
                h + window.defaultMenuSystemStyles.rowHeight *
                    d.rows > window.innerHeight && (h -= window.defaultMenuSystemStyles.rowHeight * d.rows + window.defaultMenuSystemStyles.rowHeight);
                d.xpos = g;
                d.ypos = h;
                d.parent = k;
                g = !1;
                b instanceof ControlGroup && "false" != n && b.isCollapsed && (g = !0);
                d.widthfit = !0;
                d.rowsfit = !0;
                d.initShow();
                c && (g && hideAllMenuControlBars(), r(), window[q](p));
                window.activeMenuControlBars.push(d);
                b.activeSubMenus || (b.activeSubMenus = []);
                b.activeSubMenus.push(d)
            }
        };
        k.setCallBackFn(compositeCallBackFn)
    });
    a.children("activeCode").each(function() {
        var a = window[$(this).attr("function")],
            b = $(this).attr("args");
        k.acontent = a;
        k.acArgs = b
    });
    a.attr("icon") && k.addIcon(a.attr("icon"));
    "true" == a.attr("selected") && (k.selected = !0);
    a.attr("toolTip") && (k.toolTip = a.attr("toolTip"));
    "true" == a.attr("toggleButton") && (d = new ControlButton, d.addIcon(a.attr("toggledIcon")), b.setToggleButton(k, d));
    b.addControlItem(k);
    k.parent = b;
    g && window.allNavControlButtons.push(k);
    h && window.allToolControlButtons.push(k)
}

function hideOtherSubMenus(a) {
    a.activeSubMenus && $.each(a.activeSubMenus, function() {
        for (var a = 0; a < window.activeMenuControlBars.length; a++);
        window.activeMenuControlBars.splice(a, 1);
        for (a = 0; a < window.allWindowControlBars.length; a++);
        window.allWindowControlBars.splice(a, 1);
        recurseDestroySubMenus(this);
        this.destroy()
    });
    a.activeSubMenus = []
}

function recurseDestroySubMenus(a) {
    a.activeSubMenus && $.each(a.activeSubMenus, function() {
        recurseDestroySubMenus(this)
    });
    a.destroy()
}

function inflateSelectionOptionsMenu(a, b) {
    var c = new ControlBar(0, 0, "fit", "fit");
    loadXMLMenuLayout("menus/selection_menu/layout.xml", c, null, null, function() {
        var a = b.y - window.defaultMenuSystemStyles.rowHeight * window.menuSystemScaleFactor;
        c.xpos = b.x - c.groupWidth * window.menuSystemScaleFactor;
        c.ypos = a;
        c.initShow();
        activeMenuControlBars.push(c)
    })
}

function posx(a, b, c) {
    a = document.getElementById(a).getBoundingClientRect().left;
    c.updateWidth();
    c = c.groupWidth ? c instanceof ControlBar && c.overrideWidth ? c.width : c.groupWidth : c.width;
    window.innerWidth - a < c && (a = 0 > window.innerWidth - c ? 0 : window.innerWidth - c);
    return a
}

function posy(a, b, c) {
    a = document.getElementById(a).getBoundingClientRect().top;
    a += b.height * window.menuSystemScaleFactor;
    c.updateWidth();
    window.innerHeight - a < c.height && (a = 0 > window.innerHeight - c.height ? 0 : window.innerHeight - c.height);
    return a
}

function hideAllMenuControlBars() {
    $.each(window.activeMenuControlBars, function() {
        this.isVisible && (this.parent && this.parent instanceof ControlGroup && (this.parent.menuVisible = !1), this.destroy())
    });
    window.activeMenuControlBars = []
}

function unselectTools() {
    $.each(window.allToolControlButtons, function() {
        this.selected && this.setSelected(!1)
    })
}

function unselectNavs() {
    $.each(window.allNavControlButtons, function() {
        this.selected && this.setSelected(!1)
    })
}

function updateActiveMenuBars() {
    $.each(window.allControlBars, function() {
        this.isVisible && this.updateDiv()
    })
}

function getControlBarByName(a) {
    return window.allControlBars[a]
}

function getControlGroupByName(a) {
    return window.allControlGroups[a]
}

function getControlButtonByName(a) {
    return window.allControlButtons[a]
}

function dummyFunction() {}

function drawTranspBackgrounds(a, b, c) {
    var d = b.height;
    b = b.width;
    scaling = c;
    null == scaling && (scaling = 50);
    for (c = 0; c < b; c += scaling)
        for (var e = 0; e < d; e += scaling) a.fillStyle = 0 == c / scaling % 2 ? 0 == e / scaling % 2 ? "#BBB" : "#F8F8F8" : 0 == e / scaling % 2 ? "#F8F8F8" : "#BBB", a.globalAlpha = 1, a.fillRect(c, e, c + scaling, e + scaling)
}

function setLineWidthDefaultCallback(a) {
    setLineWidth(a)
}

function setTransparencyDefaultCallback(a) {
    setTransparency(a)
}

function clearButtonDefaultCallback() {
    clearCanvas()
}

function pointerButtonDefaultCallback() {
    setBrushType(InkBrush.types.pointer)
}

function mathToolButtonDefaultCallback() {
    setBrushType(InkBrush.types.math)
}

function selectionButtonDefaultCallback() {
    setBrushType(InkBrush.types.selection)
}

function moveToolButtonDefaultCallback() {
    setBrushType(InkBrush.types.move)
}

function textToolButtonDefaultCallback() {
    setBrushType(InkBrush.types.text)
}

function eraserButtonDefaultCallback() {
    setBrushType(InkBrush.types.stroke_eraser)
}

function lineButtonDefaultCallback() {
    setBrushType(InkBrush.types.line)
}

function strokeButtonDefaultCallback() {
    setBrushType(InkBrush.types.drawing)
}

function redoButtonDefaultCallback() {
    redoDraw()
}

function undoButtonDefaultCallback() {
    undoDraw()
}

function saveToImageButtonDefaultCallback() {
    saveCanvasToPNG()
}

function saveRoomDataButtonDefaultCallback() {
    saveRoomToFile()
}

function loadFromFileDefaultCallback() {
    var a = document.createElement("DIV");
    a.style.width = "0px";
    a.style.height = "0px";
    a.style.overflow = "hidden";
    var b = document.createElement("input");
    b.type = "file";
    b.setAttribute("dialogtype", "load");
    var c = genGUID();
    b.id = c;
    a.appendChild(b);
    BODY.appendChild(a);
    $("#" + c).change(function(a) {
        getFileFromFilesystem(a)
    });
    $("#" + c).trigger("click");
    BODY.removeChild(a)
}

function increaseFontSizeDefaultCallback(a) {
    a || (a = 1);
    window.fontOptions.fontSize += a;
    updateActiveTextBoxes();
    setFontTextBoxValue()
}

function decreaseFontSizeDefaultCallback(a) {
    a || (a = 1);
    window.fontOptions.fontSize -= a;
    updateActiveTextBoxes();
    setFontTextBoxValue()
}

function setFontSizeDefaultCallback(a) {
    window.fontOption.fontSize = a;
    updateActiveTextBoxes();
    setFontTextBoxValue()
}

function onFontSizeTextChangeDefaultCallback(a, b) {
    if (a) {
        var c = a.elem;
        c && (c.onchange = function() {
            var a = parseInt(c.value);
            window.fontOptions.fontSize = a;
            setFontTextBoxValue()
        })
    }
}

function toggleBoldDefaultCallback() {
    "normal" == window.fontOptions.fontWeight ? (window.fontOptions.fontWeight = "bold", window.allControlButtons.fontSettingsBold.selected = !0) : (window.fontOptions.fontWeight = "normal", window.allControlButtons.fontSettingsBold.selected = !1);
    window.allControlButtons.fontSettingsBold.updateDiv();
    updateActiveTextBoxes()
}

function setBoldDefaultConfig(a, b, c, d, e) {
    d = "bold" == window.fontOptions.fontWeight ? !0 : !1;
    return [b, c, d]
}

function toggleItalicDefaultCallback() {
    "normal" == window.fontOptions.fontStyle ? (window.fontOptions.fontStyle = "italic", window.allControlButtons.fontSettingsItalic.selected = !0) : (window.fontOptions.fontStyle = "normal", window.allControlButtons.fontSettingsItalic.selected = !1);
    window.allControlButtons.fontSettingsItalic.updateDiv();
    updateActiveTextBoxes()
}

function setItalicDefaultConfig(a, b, c, d, e) {
    d = "italic" == window.fontOptions.fontStyle ? !0 : !1;
    return [b, c, d]
}

function toggleUnderlinedDefaultCallback() {
    "none" == window.fontOptions.textDecoration ? (window.fontOptions.textDecoration = "underline", window.allControlButtons.fontSettingsUnderlined.selected = !0) : (window.fontOptions.textDecoration = "none", window.allControlButtons.fontSettingsUnderlined.selected = !1);
    window.allControlButtons.fontSettingsUnderlined.updateDiv();
    updateActiveTextBoxes()
}

function setUnderlinedDefaultConfig(a, b, c, d, e) {
    d = "underline" == window.fontOptions.textDecoration ? !0 : !1;
    return [b, c, d]
}

function setFontTextBoxValue() {
    var a = window.allControlTextBoxes.fontSettingsCurrentFontSize.elem;
    a && (a.value = window.fontOptions.fontSize)
}

function onFontChangeDefaultCallback(a, b) {
    a && a.selectedIndex && (window.fontOptions.fontFamily = a.options[a.selectedIndex].value);
    updateActiveTextBoxes()
}

function updateFontView(a) {
    a = a.elem;
    if (a.options) {
        for (var b = window.fontOptions.fontFamily, c = 0, c = 0; c < a.options.length && b != a.options[c].value; c++);
        c == a.options.length && (c = 0);
        a.selectedIndex = c
    }
}

function updateActiveTextBoxes() {
    window.fabricCanvas.getActiveObject() && window.fabricCanvas.getActiveObject().parentObject instanceof TextBox && (window.fabricCanvas.getActiveObject().parentObject.setFontOptions(window.fontOptions), $.extend(window.fabricCanvas.getActiveObject(), window.fontOptions))
}

function gridBackgroundDefaultCallback() {
    loadBackground(0)
}

function staffBackgroundDefaultCallback() {
    loadBackground(1)
}

function linedBackgroundDefaultCallback() {
    loadBackground(2)
}

function xoBackgroundDefaultCallback() {
    loadBackground(7)
}

function blankBackgroundDefaultCallback() {
    loadBackground(8)
}

function linkAppendCallback() {
    hideAllMenuControlBars();
    commInterface && (commInterface.roomID || commInterface.joinRoom(genRoomID(), function(a) {
        a && prompt("Share the following link for the chat room:", window.location.toString())
    }))
}

function linkGenerateCallback() {
    hideAllMenuControlBars();
    if (commInterface) {
        var a = prompt("Enter a new chat room name (will join if already exists):");
        a && commInterface.joinRoom(a)
    }
}

function secureRoomIfComm() {
    commInterface && commInterface.secureRoom()
};

function initEvents() {
    $(window).on("hashchange", function() {
        commInterface && "" != window.location.hash && commInterface.joinRoom(window.location.hash.substring(1))
    });
    parent != window && ($(window).on("message", function(a) {
        commInterface && a.originalEvent.data && a.originalEvent.data.roomID && commInterface.joinRoom(a.originalEvent.data.roomID)
    }), parent.postMessage("getRoomID", "*"))
};

function saveCanvasToPNG() {
    var a = fabricCanvas.toDataURL({
        format: "png"
    });
    downloadDataURL(a, "canvas.png")
}

function saveCanvasToPNGDropbox() {
    var a = fabricCanvas.toDataURL({
        format: "png"
    });
    Dropbox.save(a, "canvas.png")
}

function saveCanvasToJPG() {
    var a = fabricCanvas.toDataURL({
        format: "jpeg"
    });
    downloadDataURL(a, "canvas.jpg")
}

function saveCanvasToJPGDropbox() {
    var a = fabricCanvas.toDataURL({
        format: "jpeg"
    });
    Dropbox.save(a, "canvas.jpg")
}

function saveCanvasToSVG() {
    var a = fabricCanvas.toSVG(),
        a = "data:image/svg+xml;base64," + btoa(a);
    downloadDataURL(a, "canvas.svg")
}

function saveCanvasToSVGDropbox() {
    var a = fabricCanvas.toSVG(),
        a = "data:image/svg+xml;base64," + btoa(a);
    Dropbox.save(a, "canvas.svg")
}

function saveTracesToInkML() {
    var a = Ink.toInkML(drawStack.getTraceList()),
        a = "data:application/inkml+xml;base64," + btoa(a);
    downloadDataURL(a, "canvas.inkml")
}

function saveTracesToInkMLDropbox() {
    var a = Ink.toInkML(drawStack.getTraceList()),
        a = "data:application/inkml+xml;base64," + btoa(a);
    Dropbox.save(a, "canvas.inkml")
}

function downloadDataURL(a, b) {
    var c = a.split(",")[0].split(":")[1].split(";")[0];
    /^((?!chrome).)*safari/i.test(navigator.userAgent) ? document.location.href = a.replace(c, "image/octet-stream") : (c = new Blob([dataURLToBuffer(a)], {
        type: c
    }), saveAs(c, b))
}

function roomToArrayBuffer() {
    var a = JSON.stringify(pageList.toObject());
    return LZString.compressToUint8Array(a).buffer
}

function saveRoomToFile() {
    var a = new Blob([roomToArrayBuffer()], {
        type: "application/octet-stream"
    });
    saveAs(a, "room.data")
}

function saveRoomToDropbox() {
    var a = "data:application/octet-stream;base64," + LZString.compressToBase64(JSON.stringify(pageList.toObject()));
    Dropbox.save(a, "room.data")
}

function getFileFromDropbox() {
    Dropbox.choose({
        linkType: "direct",
        success: function(a) {
            for (var b = 0; b < a.length; b++) {
                var c = a[b].link;
                switch (c.split(".").pop()) {
                    case "inkml":
                        loadInkMLFromURL(c);
                        break;
                    case "pdf":
                        loadPDFFromURL(c);
                        break;
                    case "data":
                        loadRoomFromURL(c);
                        break;
                    default:
                        loadImageFromURL(c)
                }
            }
        },
        multiselect: !0,
        extensions: ".svg .jpg .jpeg .png .gif .tiff .bmp .tif .inkml .pdf .data".split(" ")
    })
}

function getFileFromFilesystem(a) {
    if (0 < a.target.files.length) switch (a.target.files[0].name.split(".").pop()) {
        case "inkml":
            loadInkMLFromFile(a);
            break;
        case "pdf":
            loadBackgroundFromFile(6, a);
            break;
        case "data":
            loadRoomFromFile(a);
            break;
        default:
            loadImageFromFile(a)
    }
}

function defaultLoadFromURL() {
    var a = prompt("Please enter URL to file", "http://");
    if (null != a) switch (a.split(".").pop()) {
        case "inkml":
            a = getCORSURL(a);
            loadInkMLFromURL(a);
            break;
        case "pdf":
            a = getCORSURL(a);
            loadPDFFromURL(a);
            break;
        case "data":
            a = getCORSURL(a);
            loadRoomFromURL(a);
            break;
        default:
            a = getCORSURL(a), loadImageFromURL(a)
    }
}

function loadBackgroundFromWeb(a, b) {
    var c = getCORSURL(b);
    c ? 6 == a ? loadPDFFromURL(c) : loadBackground(a, {
        url: c
    }) : console.log("invalid url:", b)
}

function loadBackgroundFromFile(a, b) {
    if (0 < b.target.files.length) {
        var c = b.target.files[0],
            d = new FileReader;
        d.onload = 6 == a ? function() {
            loadPDFFromURL(d.result)
        } : function() {
            loadBackground(a, {
                url: d.result
            })
        };
        d.readAsDataURL(c)
    }
}

function loadBackground(a, b) {
    5 == a && (b.width = fabricCanvas.width, b.height = fabricCanvas.height);
    var c = new Background(a, b);
    drawStack.setBackground(c);
    drawStack.redraw();
    c = new AddAction([c], pageList.currPage);
    undoStack.addAction(c);
    window.commInterface && commInterface.sendAction(c)
}

function loadPDFFromURL(a) {
    var b = a,
        b = 0 == a.indexOf("data:") ? dataURLToBuffer(a) : getCORSURL(a);
    PDFJS.getDocument(b).then(function(b) {
        for (var d = 1; d <= b.pdfInfo.numPages; d++) {
            if (1 < d) {
                pageList.addPage(pageList.currPage + d - 1);
                var e = new ModifyPageListAction("add", pageList.currPage + d - 1);
                window.commInterface && commInterface.sendAction(e)
            }
            b.getPage(d).then(function(b, c, d) {
                new ContextCapturer;
                c = new Background(6, {
                    url: a,
                    pdfPageNum: c,
                    pdfPage: d,
                    _content: null
                });
                pageList.getPage(b).setBackground(c);
                b == pageList.currPage &&
                    drawStack.redraw();
                b = new AddAction([c], b);
                window.commInterface && commInterface.sendAction(b)
            }.bind(null, pageList.currPage + d - 1, d))
        }
        setPageTextBoxValue()
    })
}

function loadImageFromWeb(a) {
    var b = getCORSURL(a);
    b ? loadImageFromURL(b) : console.log("invalid url:", a)
}

function loadImageFromURL(a) {
    a = new CanvasImage(a);
    drawStack.addToFront(a);
    a = new AddAction([a], pageList.currPage);
    undoStack.addAction(a);
    window.commInterface && commInterface.sendAction(a)
}

function loadImageFromFile(a) {
    if (0 < a.target.files.length) {
        a = a.target.files[0];
        var b = new FileReader;
        b.onload = function() {
            loadImageFromURL(b.result)
        };
        b.readAsDataURL(a)
    }
}

function loadInkMLFromURL(a) {
			//	console.log("aaaaa:" + a);
    var b = new XMLHttpRequest;
    b.responseType = "text";
    b.onload = function() {
        loadInkMLString(b.response)
    };
    b.open("GET", a, !0);
    b.send()
}

function loadInkMLFromFile(a) {
    if (0 < a.target.files.length) {
        a = a.target.files[0];
        var b = new FileReader;
        b.onload = function() {
            loadInkMLString(b.result)
        };
        b.readAsText(a)
    }
}

function loadInkMLString(a) {
    if (a = Ink.fromInkML($.parseXML(a))) drawStack.addMultiple(a), a = new AddAction(a, pageList.currPage), undoStack.addAction(a), window.commInterface && commInterface.sendAction(a)
}

function loadRoomFromArrayBuffer(a) {
    a = LZString.decompressFromUint8Array(new Uint8Array(a));
    pageList = PageList.fromObject(JSON.parse(a));
    pageList.setActivePage(1);
    undoStack = new UndoStack(pageList);
    setPageTextBoxValue()
}

function loadRoomFromFile(a) {
    if (0 < a.target.files.length) {
        a = a.target.files[0];
        var b = new FileReader;
        b.onload = function() {
            loadRoomFromArrayBuffer(b.result);
            window.commInterface && commInterface.sendRoomData(b.result)
        };
        b.readAsArrayBuffer(a)
    }
}

function loadRoomFromURL(a) {
    var b = new XMLHttpRequest;
    b.responseType = "arraybuffer";
    b.onload = function() {
        loadRoomFromArrayBuffer(b.response)
    };
    b.open("GET", a, !0);
    b.send()
};
var c_inkmlNS = "http://www.w3.org/2003/InkML",
    c_xmlNS = "http://www.w3.org/XML/1998/namespace",
    c_xmlnsNS = "http://www.w3.org/2000/xmlns/",
    xmlProlog = '<?xml version="1.0" encoding="UTF-8"?>';
Ink = {
    fromInkML: function(a) {
        contexts = {};
        brushes = {};
        traces = [];
        $(a).find("inkml\\:context, context").each(function() {
            var a = InkContext.fromInkML($(this)),
                c = $(this).attr("xml:id");
            if (null == c || "" == c) c = genGUID();
            contexts[c] = a
        });
        $(a).find("inkml\\:brush, brush").each(function() {
            var a = InkBrush.fromInkML($(this)),
                c = $(this).attr("xml:id");
            c && (brushes[c] = a)
        });
        $(a).find("inkml\\:trace, trace").each(function() {
            var a = $(this).attr("contextRef"),
                c = null;
            a && (c = contexts[a.substr(1)]);
            var a = $(this).attr("brushRef"),
                d =
                null;
            a && (d = brushes[a.substr(1)]);
            c = InkTrace.fromInkML($(this), c, d);
            traces.push(c)
        });
        if (0 < traces.length) return traces
    },
    toInkML: function(a) {
        contexts = {};
        brushes = {};
        traces = {};
        $.each(a, function() {
            if (this.brush) {
                var a = genGUID();
                brushes[a] = this.brush;
                this._brushRef = "#" + a
            }
            this.context && (contexts[this.context.id] = this.context);
            traces[this.id] = this
        });
        var b = document.implementation.createDocument(c_inkmlNS, "inkml:ink");
        a = b.createAttribute("xmlns:xml");
        a.nodeValue = c_xmlNS;
        b.documentElement.setAttributeNode(a);
        var c = b.createElementNS(c_inkmlNS, "inkml:definitions");
        b.documentElement.appendChild(c);
        $.each(contexts, function(a, e) {
            var f = e.toInkML(b),
                g = b.createAttribute("xml:id");
            g.nodeValue = a;
            f.setAttributeNode(g);
            c.appendChild(f)
        });
        $.each(brushes, function(a, e) {
            var f = e.toInkML(b),
                g = b.createAttribute("xml:id");
            g.nodeValue = a;
            f.setAttributeNode(g);
            c.appendChild(f)
        });
        $.each(traces, function(a, c) {
            var f = c.toInkML(b),
                g = b.createAttribute("xml:id");
            g.nodeValue = a;
            f.setAttributeNode(g);
            b.documentElement.appendChild(f)
        });
        a = (new XMLSerializer).serializeToString(b);
        return xmlProlog + a
    }
};
InkBrush = function(a) {
    $.extend(this, a)
};
InkBrush.types = {
    drawing: 1,
    stroke_eraser: 2,
    point_eraser: 3,
    pointer: 4,
    selection: 5,
    line: 6,
    text: 7,
    math: 8,
    move: 9
};
InkBrush.tips = {
    pencil: 1,
    teardrop: 2,
    pen: 3,
    highlighter: 4
};
InkBrush.fromObject = function(a) {
    return new InkBrush(a)
};
InkBrush.fromInkML = function(a) {
    var b = new InkBrush({
        width: 2,
        color: "#000000",
        transparency: 0,
        type: InkBrush.types.drawing
    });
    $(a).find("inkml\\:brushProperty, brushProperty").each(function() {
        var a = $(this).attr("name"),
            d = $(this).attr("value"),
            e = $(this).attr("units");
        switch (a) {
            case "width":
            case "height":
                b[a] = unitsToPixels(parseFloat(d), e);
                break;
            case "transparency":
                b[a] = parseFloat(d);
                break;
            case "antiAliased":
            case "fitToCurve":
            case "ignorePressure":
                "true" == d || "1" == d || "T" == d ? b[a] = !0 : "false" == d || "0" == d || "F" ==
                    d ? b[a] = !1 : console.log("invalid boolean value:", d);
                break;
            case "shape":
                "stroke_eraser" == d && (b.type = InkBrush.types.stroke_eraser);
            default:
                b[a] = d
        }
    });
    return b
};
InkBrush.prototype = {
    toInkML: function(a) {
        var b = a.createElementNS(c_inkmlNS, "inkml:brush");
        for (prop in this)
            if (this.hasOwnProperty(prop)) {
                var c = a.createElementNS(c_inkmlNS, "inkml:brushProperty");
                c.setAttribute("name", prop);
                c.setAttribute("value", this[prop]);
                b.appendChild(c)
            }
        return b
    },
    change: function(a) {
        $.extend(this, a)
    },
    isDrawing: function() {
        return this.type == InkBrush.types.drawing || this.type == InkBrush.types.line
    }
};
InkTrace = function(a) {
    DrawableObject.call(this);
    this.type = "inktrace";
    a.brush && a.brush instanceof Object && (a.brush = new InkBrush(a.brush));
    $.extend(this, a)
};
InkTrace.fromDrawing = function(a, b) {
    return new InkTrace({
        values: [a],
        brush: cloneObject(b),
        complete: !1
    })
};
InkTrace.fromObject = function(a) {
    return new InkTrace(a)
};
InkTrace.fromInkML = function(a, b, c) {
    var d = new InkTrace({
        values: []
    });
    d.id = a.attr("xml:id");
    d.id || (d.id = genGUID());
    c ? d.brush = c : b.brush && (d.brush = b.brush);
    b && (d.context = b);
    a = a.text().split(",");
    for (b = 0; b < a.length; b++) c = a[b].trim().split(/\s+/), d.addPoint({
        x: c[0],
        y: c[1]
    });
    d.completeTrace();
    return d
};
InkTrace.prototype = $.extend(Object.create(DrawableObject.prototype), {
    addPoint: function(a) {
        this.values.push(a)
    },
    replaceLastPoint: function(a) {
        this.values.length = 1;
        this.addPoint(a)
    },
    getLastPoint: function() {
        return this.values[this.values.length - 1]
    },
    completeTrace: function() {
        this.complete = !0;
        this.xpos = this.values[0].x;
        this.ypos = this.values[0].y;
        for (var a = 1; a < this.values.length; a++) this.values[a].x < this.xpos && (this.xpos = this.values[a].x), this.values[a].y < this.ypos && (this.ypos = this.values[a].y);
        for (a = 0; a <
            this.values.length; a++) this.values[a].x -= this.xpos, this.values[a].y -= this.ypos;
        this.xpos -= this.brush.width / 2;
        this.ypos -= this.brush.width / 2
    },
    intersectsBox: function(a) {
        if (!DrawableObject.prototype.intersectsBox.call(this, a)) return !1;
        var b = this,
            c = [];
        $.each(a, function() {
            c.push({
                x: this.x - b.xpos,
                y: this.y - b.ypos
            })
        });
        if (1 == this.values.length && (a = traceSegToRect(this.values[0], this.values[0], this.brush.width, this.scaleX, this.scaleY, this.angle), boxesIntersect(c, a))) return !0;
        for (var d = 0; d < this.values.length -
            1; d++)
            if (a = traceSegToRect(this.values[d], this.values[d + 1], this.brush.width, this.scaleX, this.scaleY, this.angle), boxesIntersect(c, a)) return !0;
        return !1
    },
    toInkML: function(a) {
        a || (a = document.implementation.createDocument(c_inkmlNS, "inkml:ink", null));
        var b = a.createElementNS(c_inkmlNS, "inkml:trace");
        this.timeOffset && b.setAttribute("timeOffset", this.timeOffset);
        this.brush && (b.setAttribute("brushRef", this._brushRef), delete this._brushRef);
        this.context && b.setAttribute("contextRef", "#" + this.context.id);
        var c =
            this,
            d = "";
        $.each(this.values, function() {
            d.length && (d += ",");
            d += c.xpos + c.brush.width / 2 + this.x + " " + (c.ypos + c.brush.width / 2 + this.y)
        });
        a = a.createTextNode(d);
        b.appendChild(a);
        return b
    },
    render: function(a) {
        var b = [];
        b.push(["M", this.values[0].x, this.values[0].y]);
        1 == this.values.length && b.push(["L", this.values[0].x + .5, this.values[0].y]);
        for (var c = 1; c < this.values.length; c++) b.push(["L", this.values[c].x, this.values[c].y]);
        c = this.getFabricOptions(a);
        b = new fabric.Path(b, c);
        b.parentObject = this;
        b.left = c.left;
        b.top =
            c.top;
        this.complete || (b.pathOffset = {
            x: 0,
            y: 0
        }, b.left -= this.brush.width / 2 * a.zoom, b.top -= this.brush.width / 2 * a.zoom);
        b.setCoords();
        return b
    },
    getFabricOptions: function(a) {
        return $.extend({
            stroke: this.brush.color,
            strokeWidth: this.brush.width,
            opacity: 1 - this.brush.transparency,
            strokeLineCap: "round",
            strokeLineJoin: "round",
            fill: null
        }, DrawableObject.prototype.getFabricOptions.call(this, a))
    }
});
InkContext = function() {};
InkContext.fromInkML = function(a) {
    var b = new InkContext;
    b.inkSource = null;
    b.brush = null;
    b.timestamp = null;
    b.xFactor = 1;
    b.yFactor = 1;
    b.fFactor = 1;
    b.fNeutral = .5;
    var c = $(a).find("inkml\\:inkSource, inkSource");
    c.length && (b.inkSource = new InkSource(c));
    c = $(a).find("inkml\\:brush, brush");
    c.length && (b.brush = InkBrush.fromInkML(c));
    a = $(a).find("inkml\\:timestamp, timestamp");
    a.length && (b.timestamp = new InkTimestamp(a));
    return b
};
InkContext.prototype = {
    toInkML: function(a) {
        var b = a.createElementNS(c_inkmlNS, "inkml:context");
        if (this.inkSource) {
            var c = this.inkSource.toInkML(a);
            b.appendChild(c)
        }
        this.timestamp && (a = this.timestamp.toInkML(a), b.appendChild(a));
        return b
    }
};
InkSource = function(a) {
    this.init(a)
};
$.extend(InkSource.prototype, {
    init: function(a) {
        this.id = $(a).attr("xml:id");
        null == this.id && (this.id = $(a).attr("id"));
        this.traceFormat = null;
        this.channelProperties = [];
        var b = $(a).find("inkml\\:traceFormat, traceFormat");
        1 > b.length && alert("error: traceFormat is required on inkSource");
        var c = $(a).find("inkml\\:channelProperties, channelProperties");
        this.traceFormat = new InkTraceFormat(b, c);
        this.channels = {};
        var d = this;
        $(a).find("inkml\\:channelProperty, channelProperty").each(function() {
            var a = new InkChannelProperty($(this));
            d.channelProperties.push(a)
        })
    },
    toInkML: function(a) {
        var b = a.createElementNS(c_inkmlNS, "inkml:inkSource"),
            c = a.createAttribute("xml:id");
        c.nodeValue = this.id;
        b.setAttributeNode(c);
        c = this.traceFormat.toInkML(a);
        b.appendChild(c);
        var d = a.createElementNS(c_inkmlNS, "inkml:channelProperties");
        b.appendChild(d);
        $.each(this.channelProperties, function(b, c) {
            var g = c.toInkML(a);
            d.appendChild(g)
        });
        return b
    }
});
InkTimestamp = function(a) {
    this.init(a)
};
$.extend(InkTimestamp.prototype, {
    init: function(a) {
        this.id = $(a).attr("xml:id");
        null == this.id && (this.id = $(a).attr("id"));
        this.timeString = $(a).attr("timeString")
    },
    toInkML: function(a) {
        var b = a.createElementNS(c_inkmlNS, "inkml:timestamp");
        a = a.createAttribute("xml:id");
        a.nodeValue = this.id;
        b.setAttributeNode(a);
        b.setAttribute("timeString", this.timeString);
        return b
    }
});
InkTraceFormat = function(a, b) {
    this.init(a, b)
};
$.extend(InkTraceFormat.prototype, {
    init: function(a, b) {
        this.id = $(a).attr("xml:id");
        null == this.id && (this.id = $(a).attr("id"));
        this.channels = {};
        var c = this;
        $(a).find("inkml\\:channel, channel").each(function() {
            var a = $(this).attr("name"),
                e = new InkChannel($(this), b);
            c.channels[a] = e
        })
    },
    toInkML: function(a) {
        var b = a.createElementNS(c_inkmlNS, "inkml:traceFormat"),
            c = a.createAttribute("xml:id");
        c.nodeValue = this.id;
        b.setAttributeNode(c);
        $.each(this.channels, function(c, e) {
            var f = e.toInkML(a);
            b.appendChild(f)
        });
        return b
    }
});
InkChannel = function(a, b) {
    this.init(a, b)
};
$.extend(InkChannel.prototype, {
    init: function(a, b) {
        this.name = a.attr("name");
        this.type = a.attr("type");
        var c = a.attr("min");
        this.min = null == c ? 0 : parseFloat(c);
        this.max = parseFloat(a.attr("max"));
        this.units = a.attr("units");
        this.resolution = 0;
        var d = b.find("inkml\\:channelProperty[channel='" + this.name + "'][name='resolution'], channelProperty[channel='" + this.name + "'][name='resolution']");
        d.length && (c = d.attr("value"), d = d.attr("units"), "1/" != d.substr(0, 2) && alert("error: units of resolution property expected to be 1/unit"), d =
            d.substring(2), this.units != d && alert("error: units of resolution property expected to be same as channel"), this.resolution = c)
    },
    toInkML: function(a) {
        a = a.createElementNS(c_inkmlNS, "inkml:channel");
        a.setAttribute("name", this.name);
        a.setAttribute("type", this.type);
        0 != this.min && a.setAttribute("min", this.min);
        a.setAttribute("max", this.max);
        a.setAttribute("units", this.units);
        return a
    }
});
InkChannelProperty = function(a) {
    this.init(a)
};
$.extend(InkChannelProperty.prototype, {
    init: function(a) {
        this.channel = a.attr("channel");
        this.name = a.attr("name");
        this.value = parseFloat(a.attr("value"));
        this.units = a.attr("units")
    },
    toInkML: function(a) {
        a = a.createElementNS(c_inkmlNS, "inkml:channelProperty");
        a.setAttribute("channel", this.channel);
        a.setAttribute("name", this.name);
        a.setAttribute("value", this.value);
        a.setAttribute("units", this.units);
        return a
    }
});
LoaderDialog = function(a) {
    this.totalLength = a;
    this.progress = 0
};
LoaderDialog.prototype = {
    start: function() {
        document.getElementById("bodyContainer").style.visibility = "hidden";
        var a = document.getElementById("mainBody"),
            b = document.createElement("div"),
            c = document.createElement("img");
        c.src = "inkchat_logo.png";
        c.style.width = "200px";
        c.style.height = "200px";
        a.style.backgroundColor = "FFFFFF";
        var d = window.innerHeight / 2,
            e = window.innerWidth / 2;
        c.style.top = d - 80 - 10 + "px";
        c.style.left = e - 80 + "px";
        c.style.position = "absolute";
        c.style.height = "160px";
        c.style.width = "160px";
        c.width = 160;
        c.height =
            160;
        c.id = "logoLoader";
        b.appendChild(c);
        c = document.createElement("canvas");
        c.style.width = "200px";
        c.style.height = "10px";
        c.width = 200;
        c.height = 10;
        c.id = "loaderPosition";
        c.style.top = d + 90 + "px";
        c.style.position = "absolute";
        c.style.left = e - 100 + "px";
        d = c.getContext("2d");
        d.beginPath();
        d.fillStyle = "#CCCCCC";
        d.lineWidth = 10;
        d.lineCap = "round";
        d.moveTo(5, 5);
        d.lineTo(195, 5);
        d.stroke();
        b.appendChild(c);
        b.id = "loaderContainer";
        a.appendChild(b)
    },
    end: function() {
        var a = document.getElementById("loaderContainer");
        $(a).fadeOut(100,
            function() {
                a.parentNode.removeChild(a)
            });
        document.getElementById("bodyContainer").style.visibility = "visible"
    },
    step: function() {
        this.progress++;
        this._updateCompletion(this.progress / this.totalLength)
    },
    _updateCompletion: function(a) {
        var b = document.getElementById("loaderPosition").getContext("2d");
        b.beginPath();
        b.fillStyle = "#666666";
        b.lineWidth = 10;
        b.lineCap = "round";
        b.moveTo(5, 5);
        b.lineTo(195 * a, 5);
        b.stroke()
    }
};
(function() {
    window.postMainControlBarLoad.push(function() {
        window.microWriter = new MicroWriter(100, 100, 4);
        var a = new ControlButton(function() {
            window.microWriter.renderElement()
        });
        a.addIcon("./menus/icons/microWriter.svg");
        a.toolTip = "microWriter Tool";
        window.allControlButtons.microWriterToolSelect = a;
        a.parent = window.mainControlBar;
        window.allControlGroups.brushTypes.addControlItemToFront(a);
        updateActiveMenuBars()
    })
})();
MicroWriter = function(a, b, c) {
    this.id = genGUID();
    this.generateElement();
    this.y = this.x = 50;
    a && b && (this.x = a, this.y = b);
    this.zoom = 4;
    c && (this.zoom = c);
    this.textStartRect = this.elemRect = this.fabricCanvasRect = this.miniWidth = this.miniHeight = null;
    this.backgroundColor = window.defaultMenuSystemStyles.menuBackgroundColor;
    this.position = "fixed";
    this.canvasHeightFactor = "50";
    this.canvasPositionFactor = "25";
    this.textStartCoverWidthFactor = "15";
    this.textStartCoverAlpha = "0.2";
    this.sizeFactor = "60";
    this.canvasCoverAlpha = "0.2";
    this.close = new ControlButton(this.destroy, this);
    this.zoomIn = new ControlButton(this._zoomIn, this);
    this.zoomOut = new ControlButton(this._zoomOut, this);
    this.moveLeft = new ControlButton(this._moveLeft, this);
    this.moveRight = new ControlButton(this._moveRight, this);
    this.moveUp = new ControlButton(this._moveUp, this);
    this.moveDown = new ControlButton(this._moveDown, this);
    this.close.addIcon("./menus/icons/img_icon_close.svg");
    this.zoomIn.addIcon("./menus/icons/zoom_in.svg");
    this.zoomOut.addIcon("./menus/icons/zoom_out.svg");
    this.moveLeft.addIcon("./menus/icons/left.svg");
    this.moveRight.addIcon("./menus/icons/right.svg");
    this.moveUp.addIcon("./menus/icons/collapse.svg");
    this.moveDown.addIcon("./menus/icons/expand.svg");
    this.buttonTopMargin = 10;
    this.toggledStatusBar = !1
};
MicroWriter.prototype = {
    _zoomIn: function(a) {
        a.zoom++;
        a.setZoomFactor(a.zoom)
    },
    _zoomOut: function(a) {
        a.zoom--;
        a.setZoomFactor(a.zoom)
    },
    _moveLeft: function(a) {
        a.setMainCanvasPosition(a.x - a.miniWidth * (1 - a.textStartCoverWidthFactor / 100), a.y)
    },
    _moveRight: function(a) {
        a.setMainCanvasPosition(a.x + a.miniWidth * (1 - a.textStartCoverWidthFactor / 100), a.y)
    },
    _moveUp: function(a) {
        a.setMainCanvasPosition(a.x, a.y - a.miniHeight * (1 - a.textStartCoverWidthFactor / 100))
    },
    _moveDown: function(a) {
        a.setMainCanvasPosition(a.x, a.y + a.miniHeight *
            (1 - a.textStartCoverWidthFactor / 100))
    },
    mouseUp: function(a, b) {
        a.clientX > BODY.clientWidth - .08 * BODY.clientWidth && b._moveRight(b)
    },
    generateElement: function() {
        this.elem = document.createElement("div");
        this.elem.id = this.id;
        this.canvas = document.createElement("canvas");
        this.canvas.id = genGUID();
        this.textStartCover = document.createElement("div");
        this.textStartCover.id = genGUID();
        this.canvasCover = document.createElement("canvas");
        this.canvasCover.id = genGUID();
        this.elem.style.backgroundColor = this.backgroundColor;
        this.elem.style.position =
            this.position;
        this.elem.style.zIndex = "50";
        this.textStartCover.style.zIndex = "52";
        this.setDimensions()
    },
    setDimensions: function() {
        this.aspect = aspectIsHorizontal() ? "h" : "v";
        "h" == this.aspect ? (this.height = window.innerHeight, this.width = this.sizeFactor + "%", this.elem.style.height = this.height + "px", this.elem.style.width = this.width, this.elem.style.right = "0%") : (this.width = window.innerWidth, this.height = this.sizeFactor + "%", this.elem.style.height = this.height, this.elem.style.width = this.width + "px", this.elem.style.bottom =
            "0%");
        this.textStartCover.style.position = "fixed";
        this.textStartCover.style.width = this.textStartCoverWidthFactor + "%";
        this.textStartCover.style.height = this.canvasHeightFactor + "%";
        "h" == this.aspect ? (this.textStartCover.style.top = this.canvasPositionFactor + "%", this.textStartCover.style.right = this.sizeFactor - this.textStartCoverWidthFactor + "%") : (this.textStartCover.style.bottom = "10%", this.textStartCover.style.left = "0%");
        this.textStartCover.style.backgroundColor = "rgba(102,102,102," + this.textStartCoverAlpha +
            ")"
    },
    resetDimensions: function() {
        this.destroy();
        this.renderElement()
    },
    setUpCanvas: function() {
        BODY.appendChild(this.canvas);
        var a = this.elem.getBoundingClientRect();
        this.viewport = {
            ox: 0,
            oy: 0,
            x: this.x,
            y: this.y,
            zoom: this.zoom
        };
        this.fabricCanvas = initCanvas(this.canvas, this.canvas.width, this.canvas.height, this.viewport);
        this.fabricCanvas.wrapperEl.style.position = "fixed";
        "h" == this.aspect ? (this.fabricCanvas.wrapperEl.style.width = this.sizeFactor - .2 * this.sizeFactor + "%", this.fabricCanvas.wrapperEl.style.top = this.canvasPositionFactor +
            "%", this.fabricCanvas.wrapperEl.style.right = .1 * this.sizeFactor + "%", this.fabricCanvas.wrapperEl.style.height = "65%") : (this.fabricCanvas.wrapperEl.style.width = "80%", this.fabricCanvas.wrapperEl.style.height = this.canvasHeightFactor + "%", this.fabricCanvas.wrapperEl.style.left = "10%", this.fabricCanvas.wrapperEl.style.top = "45%");
        this.fabricCanvas.wrapperEl.style.zIndex = "51";
        var b = this.fabricCanvas.wrapperEl.getBoundingClientRect();
        this.fabricCanvas.setWidth(b.right - b.left);
        this.fabricCanvas.setHeight(b.bottom -
            b.top);
        var c = this;
        $(this.fabricCanvas.wrapperEl).on("mouseup", function(a) {
            c.mouseUp(a, c)
        });
        this.fabricCanvasRect = b;
        this.elemRect = a;
        drawStack.redraw();
        BODY.appendChild(this.canvasCover);
        this.updateCanvasCover()
    },
    updateCanvasCover: function() {
        var a = this.fabricCanvas.wrapperEl.getBoundingClientRect(),
            b = this.elem.getBoundingClientRect();
        this.fabricCanvasRect = a;
        this.elemRect = b;
        var b = this.x,
            c = this.y;
        this.canvasCover.style.zIndex = "2";
        this.canvasCover.style.position = "fixed";
        this.canvasCover.style.width = window.innerWidth +
            "px";
        this.canvasCover.style.height = window.innerHeight + "px";
        this.canvasCover.style.top = "0px";
        this.canvasCover.style.left = "0px";
        this.canvasCover.width = window.innerWidth;
        this.canvasCover.height = window.innerHeight;
        var d = this.canvasCover.getContext("2d"),
            e = a.height / this.zoom;
        this.miniHeight = e;
        this.miniWidth = a = a.width / this.zoom;
        d.fillStyle = "rgba(102,102,102, 0.1)";
        d.fillRect(0, 0, window.innerWidth, c - window.fabricCanvases[0].viewport.y);
        d.fillRect(0, c - window.fabricCanvases[0].viewport.y + e, window.innerWidth,
            window.innerHeight);
        d.fillRect(0, c - window.fabricCanvases[0].viewport.y, b - window.fabricCanvases[0].viewport.x, e);
        d.fillRect(b - window.fabricCanvases[0].viewport.x + a, c - window.fabricCanvases[0].viewport.y, window.innerWidth, e);
        d.strokeStyle = "rgb(102,102,102)";
        d.shadow = "rgb(0,0,0)";
        d.shadowBlur = 10;
        d.strokeRect(b - window.fabricCanvases[0].viewport.x, c - window.fabricCanvases[0].viewport.y, a, e)
    },
    dragLogic: function() {
        var a = fabricCanvases[0].viewport;
        this.dragBox = {
            x1: this.x - a.x,
            y1: this.y - a.y,
            x2: this.x + this.miniWidth -
                a.x,
            y2: this.y + this.miniHeight - a.y
        };
        var b = this,
            c = function(c) {
                var e, f;
                "mousemove" == c.type && (e = c.clientX, f = c.clientY);
                "touchmove" == c.type && (e = c.originalEvent.touches[0].clientX, f = c.originalEvent.touches[0].clientY);
                b.dragBox.prevX && b.dragBox.prevY ? (b.x += e - b.dragBox.prevX, b.y += f - b.dragBox.prevY, b.dragBox = {
                    x1: b.x - a.x,
                    y1: b.y - a.y,
                    x2: b.x + b.miniWidth - a.x,
                    y2: b.y + b.miniHeight - a.y,
                    prevX: e,
                    prevY: f
                }, b.setMainCanvasPosition(b.x, b.y)) : b.dragBox = {
                    x1: b.x - a.x,
                    y1: b.y - a.y,
                    x2: b.x + b.miniWidth - a.x,
                    y2: b.y + b.miniHeight - a.y,
                    prevX: e,
                    prevY: f
                }
            };
        $("#" + b.canvasCover.id).on("mousedown", function(d) {
            d.clientX && d.clientY && d.clientX >= b.dragBox.x1 && d.clientX <= b.dragBox.x2 && d.clientY >= b.dragBox.y1 && d.clientY <= b.dragBox.y2 && (b.dragBox = {
                x1: b.x - a.x,
                y1: b.y - a.y,
                x2: b.x + b.miniWidth - a.x,
                y2: b.y + b.miniHeight - a.y,
                prevX: d.clientX,
                prevY: d.clientY
            }, $("#" + b.canvasCover.id).on("mousemove", c), $("#" + b.canvasCover.id).on("mouseup", function(a) {
                $("#" + b.canvasCover.id).off("mousemove");
                $("#" + b.canvasCover.id).off("mouseup")
            }))
        });
        $("#" + b.canvasCover.id).on("touchstart",
            function(d) {
                d.originalEvent.touches[0].clientX && d.originalEvent.touches[0].clientY && d.originalEvent.touches[0].clientX >= b.dragBox.x1 && d.originalEvent.touches[0].clientX <= b.dragBox.x2 && d.originalEvent.touches[0].clientY >= b.dragBox.y1 && d.originalEvent.touches[0].clientY <= b.dragBox.y2 && (b.dragBox = {
                    x1: b.x - a.x,
                    y1: b.y - a.y,
                    x2: b.x + b.miniWidth - a.x,
                    y2: b.y + b.miniHeight - a.y,
                    prevX: d.originalEvent.touches[0].clientX,
                    prevY: d.originalEvent.touches[0].clientY
                }, $("#" + b.canvasCover.id).on("touchmove", c), $("#" + b.canvasCover.id).on("touchend",
                    function(a) {
                        $("#" + b.canvasCover.id).off("touchmove");
                        $("#" + b.canvasCover.id).off("touchend")
                    }))
            })
    },
    resetDragLogic: function() {
        this.canvasCover && ($("#" + this.canvasCover.id).off("mousedown"), $("#" + this.canvasCover.id).off("mousemove"), $("#" + this.canvasCover.id).off("mouseup"), $("#" + this.canvasCover.id).off("touchstart"), $("#" + this.canvasCover.id).off("touchmove"), $("#" + this.canvasCover.id).off("touchend"), this.dragLogic())
    },
    setZoomFactor: function(a) {
        this.zoom = a;
        this.viewport.zoom = a;
        editFabricCanvasViewport(this.fabricCanvas,
            this.viewport);
        this.updateCanvasCover();
        drawStack.redraw()
    },
    setMainCanvasPosition: function(a, b) {
        this.x = a;
        this.y = b;
        this.viewport.x = a;
        this.viewport.y = b;
        var c = fabricCanvases[0].viewport;
        if ("h" == this.aspect) {
            if (this.x < c.x) {
                var d = fabricCanvases[0].viewport,
                    e = c.x - this.x - this.miniWidth;
                d.x = d.x - this.elemRect.left / d.zoom - e + 1;
                fabricCanvases[0].viewport = d;
                editFabricCanvasViewport(fabricCanvases[0].canvas, fabricCanvases[0].viewport);
                this.resetDragLogic()
            } else this.x > c.x + this.elemRect.left - this.miniWidth && (d =
                fabricCanvases[0].viewport, e = this.x - (c.x + this.elemRect.left + this.miniWidth), d.x = d.x + this.elemRect.left / d.zoom + e - 1 + this.miniWidth, fabricCanvases[0].viewport = d, editFabricCanvasViewport(fabricCanvases[0].canvas, fabricCanvases[0].viewport), this.resetDragLogic());
            this.y < c.y ? (d = fabricCanvases[0].viewport, e = c.y - this.y - this.miniHeight, d.y = d.y - window.innerHeight / d.zoom - e + 1, fabricCanvases[0].viewport = d, editFabricCanvasViewport(fabricCanvases[0].canvas, fabricCanvases[0].viewport), this.resetDragLogic()) : this.y >
                c.y + window.innerHeight - this.miniWidth && (d = fabricCanvases[0].viewport, e = this.y - (c.y + window.innerHeight + this.miniHeight), d.y = d.y + window.innerHeight / d.zoom + e - 1 + this.miniHeight, fabricCanvases[0].viewport = d, editFabricCanvasViewport(fabricCanvases[0].canvas, fabricCanvases[0].viewport), this.resetDragLogic());
            window.canvasPositionDebug && (window.canvasPositionDebug.innerHTML = "(horizontal)x: " + fabricCanvases[0].viewport.x + "y: " + fabricCanvases[0].viewport.y)
        } else this.x < c.x ? (d = fabricCanvases[0].viewport, e = c.x -
                this.x - this.miniWidth, d.x = d.x - window.innerWidth / d.zoom - e + 1, fabricCanvases[0].viewport = d, editFabricCanvasViewport(fabricCanvases[0].canvas, fabricCanvases[0].viewport), this.resetDragLogic()) : this.x > c.x + window.innerWidth - this.miniWidth && (d = fabricCanvases[0].viewport, e = this.x - (c.x + window.innerWidth + this.miniWidth), d.x = d.x + window.innerWidth / d.zoom + e - 1 + this.miniWidth, console.log(this.elemRect), fabricCanvases[0].viewport = d, editFabricCanvasViewport(fabricCanvases[0].canvas, fabricCanvases[0].viewport), this.resetDragLogic()),
            this.y < c.y ? (d = fabricCanvases[0].viewport, e = c.y - this.y - this.miniHeight, d.y = d.y - this.elemRect.top / d.zoom - e + 1, fabricCanvases[0].viewport = d, editFabricCanvasViewport(fabricCanvases[0].canvas, fabricCanvases[0].viewport), this.resetDragLogic()) : this.y > c.y + this.elemRect.top - this.miniWidth && (d = fabricCanvases[0].viewport, e = this.y - (c.y + this.elemRect.top + this.miniHeight), d.y = d.y + this.elemRect.top / d.zoom + e - 1 + this.miniHeight, fabricCanvases[0].viewport = d, editFabricCanvasViewport(fabricCanvases[0].canvas, fabricCanvases[0].viewport),
                this.resetDragLogic()), window.canvasPositionDebug && (window.canvasPositionDebug.innerHTML = "(verticle)x: " + fabricCanvases[0].viewport.x + "y: " + fabricCanvases[0].viewport.y);
        editFabricCanvasViewport(this.fabricCanvas, this.viewport);
        this.updateCanvasCover();
        drawStack.redraw()
    },
    setUpButtons: function() {
        "h" == this.aspect ? (this.prepControlButton(this.close, this.close.baseCallback, this.buttonTopMargin, 5), this.prepControlButton(this.zoomIn, this.zoomIn.baseCallback, this.buttonTopMargin, 15), this.prepControlButton(this.zoomOut,
            this.zoomOut.baseCallback, this.buttonTopMargin, 25), this.prepControlButton(this.moveUp, this.moveUp.baseCallback, 15, 45), this.prepControlButton(this.moveDown, this.moveDown.baseCallback, 90, 45), this.prepControlButton(this.moveRight, this.moveRight.baseCallback, 50, 0), this.prepControlButton(this.moveLeft, this.moveLeft.baseCallback, 50, 90)) : (this.prepControlButton(this.close, this.close.baseCallback, 0, 5), this.prepControlButton(this.zoomIn, this.zoomIn.baseCallback, 0, 15), this.prepControlButton(this.zoomOut, this.zoomOut.baseCallback,
            0, 25), this.prepControlButton(this.moveUp, this.moveUp.baseCallback, 0, 45), this.prepControlButton(this.moveDown, this.moveDown.baseCallback, -90, 45), this.prepControlButton(this.moveRight, this.moveRight.baseCallback, -45, 0), this.prepControlButton(this.moveLeft, this.moveLeft.baseCallback, -45, 90))
    },
    prepControlButton: function(a, b, c, d) {
        var e = this.elem.getBoundingClientRect(),
            f = e.right - e.left,
            e = e.bottom - e.top;.1 * f > .1 * e ? (a.width = .1 * e, a.height = .1 * e) : (a.width = .1 * f, a.height = .1 * f);
        a.generateElement();
        a.elem.style.position =
            "fixed";
        a.elem.style.zIndex = "60";
        a.elem.style.top = "h" == this.aspect ? this.elemRect.top + e * c / 100 + "px" : this.elemRect.top - e * c / 100 + "px";
        a.elem.style.right = f * d / 100 + "px";
        $("#" + this.elem.id).prepend(a.elem);
        var g = this;
        $(a.elem).unbind();
        $(a.elem).click(function() {
            b(g)
        })
    },
    computePosition: function() {
        var a = this.fabricCanvas.wrapperEl.getBoundingClientRect();
        this.viewport.ox = a.left;
        this.viewport.oy = a.top;
        editFabricCanvasViewport(this.fabricCanvas, this.viewport)
    },
    renderElement: function() {
        fabricCanvas.freeDrawingBrush instanceof
        ViewportBrush && $("#" + window.allControlButtons.brushTypesDrawingToolMenuItem.id).click();
        this.generateElement();
        BODY.appendChild(this.elem);
        this.setUpCanvas();
        this.dragLogic();
        this.setMainCanvasPosition(this.x, this.y);
        this.computePosition();
        this.setUpButtons();
        fabricCanvas.freeDrawingBrush.dispose && fabricCanvas.freeDrawingBrush.dispose();
        $("#" + window.allControlButtons.brushTypesDrawingToolMenuItem.id).click();
        window.allControlButtons.microWriterToolSelect.enabled = !1;
        window.allControlButtons.brushTypesPointerMenuItem.enabled = !1;
        window.allControlButtons.brushTypesMathMenuItem.enabled = !1;
        window.allControlButtons.brushTypesTextMenuItem.enabled = !1;
        window.allControlButtons.microWriterToolSelect.updateDiv();
        window.statusBar.isVisible && (window.statusBar.toggle(), this.toggledStatusBar = !0);
        var a = this;
        $(window).resize(function() {
            a.destroy(a)
        })
    },
    destroy: function(a) {
        var b = null;
        this.canvasCover && ($("#" + this.canvasCover.id).off("mousedown"), $("#" + this.canvasCover.id).off("mousemove"), $("#" + this.canvasCover.id).off("mouseup"), $("#" +
            this.canvasCover.id).off("touchstart"), $("#" + this.canvasCover.id).off("touchmove"), $("#" + this.canvasCover.id).off("touchend"));
        $.each(window.fabricCanvases, function(c, d) {
            d.canvas == a.fabricCanvas && (b = c)
        });
        b && fabricCanvases.splice(b, 1);
        window.allControlButtons.microWriterToolSelect.enabled = !0;
        window.allControlButtons.brushTypesPointerMenuItem.enabled = !0;
        window.allControlButtons.brushTypesMathMenuItem.enabled = !0;
        window.allControlButtons.brushTypesTextMenuItem.enabled = !0;
        window.allControlButtons.microWriterToolSelect.updateDiv();
        a.toggledStatusBar && (window.statusBar.toggle(), a.toggledStatusBar = !1);
        reInitMainCanvasListeners(fabricCanvases[0].canvas);
        try {
            BODY.removeChild(a.elem)
        } catch (c) {}
        try {
            BODY.removeChild(a.textStartCover)
        } catch (d) {}
        try {
            BODY.removeChild(a.canvasCover)
        } catch (e) {}
        try {
            BODY.removeChild(a.fabricCanvas.wrapperEl)
        } catch (f) {}
    }
};
window.googleInkchatAuthorization = {
    loadGAPIForAuth: function() {
        var a = document.createElement("script");
        a.type = "text/javascript";
        a.async = !0;
        a.src = "https://apis.google.com/js/client:plusone.js?onload=googleAuthorizationRender";
        var b = document.getElementsByTagName("script")[0];
        b.parentNode.insertBefore(a, b)
    },
    googleAuthorizationRenderNow: function() {
        gapi.auth.signIn({
            callback: window.googleInkchatAuthorization.callback
        })
    },
    callback: function(a) {
        a.status.signed_in ? gapi.client.load("plus", "v1", function() {
            gapi.client.plus.people.get({
                userId: "me"
            }).execute(function(a) {
                window.googleInkchatAuthorization.profile =
                    a;
                configureForGoogleLoggedIn()
            })
        }) : window.googleInkchatAuthorization.logout("failed")
    },
    logout: function(a) {
        "failed" == a && (hideAllMenuControlBars(), window.googleInkchatAuthorization.loggedIn = !1);
        a || (hideAllMenuControlBars(), window.user = {
            name: "Guest - " + genGUID().substring(0, 8),
            authenticated: !1,
            picture: "menus/icons/user.svg"
        }, commInterface && commInterface.updateUser(), gapi.auth.signOut(), regenLoginCGroup(window.googleInkchatAuthorization.cGroupOld), window.googleInkchatAuthorization.loggedIn = !1)
    }
};

function googleAuthorizationRender() {
    var a = {
            callback: window.googleInkchatAuthorization.callback
        },
        b = window.allControlButtons.googleLoginMenuItem.elem;
    b && b.addEventListener("click", function() {
        gapi.auth.signIn(a)
    })
}

function regenLoginCGroup(a) {
    var b = window.allControlGroups.signInStatusItem;
    b.controlItems = [];
    window.allControlButtons.googleLoginMenuItem.callbackFn = function() {
        hideAllMenuControlBars();
        googleLoginNow()
    };
    window.allControlButtons.fbookLoginMenuItem.callbackFn = function() {
        hideAllMenuControlBars();
        faceBookLoginNow()
    };
    $.each(a, function() {
        b.addControlItem(this)
    });
    b.addIcon(window.user.picture);
    window.allControlButtons.secureRoom.enabled = !1;
    window.statusBar.updateDiv()
}

function configureForGoogleLoggedIn() {
    if (!window.googleInkchatAuthorization.loggedIn) {
        window.googleInkchatAuthorization.loggedIn = !0;
        hideAllMenuControlBars();
        var a = window.allControlGroups.signInStatusItem,
            b = window.googleInkchatAuthorization.profile;
        window.user.uuid = b.id;
        window.user.name = b.displayName;
        window.user.picture = b.image.url;
        window.user.authenticated = !0;
        window.commInterface.updateUser();
        window.allControlButtons.secureRoom.enabled = !0;
        a.addIcon(b.image.url);
        window.googleInkchatAuthorization.cGroupOld =
            a.controlItems;
        a.controlItems = [];
        a.addControlItem(new ControlTextField(unitsToPixels(2.5, "cm"), b.displayName, 12));
        b = new ControlButton(window.googleInkchatAuthorization.logout);
        b.addIcon("menus/icons/enter_room.svg");
        a.addControlItem(b);
        a.updateWidth();
        a.collapsedButton.callbackFn = function() {
            a.callbackFn(a)
        };
        window.statusBar.updateDiv()
    }
}
window.fbAuthorizationWrapper = {
    profile: {
        name: "unknown",
        image: "menus/icons/user.svg"
    },
    loginButtonCallback: function() {
        FB.login(function(a) {
            window.fbAuthorizationWrapper.statusChangeCallback(a)
        })
    },
    statusChangeCallback: function(a) {
        console.log("statusChangeCallback");
        "connected" === a.status ? FB.api("/me", function(a) {
            console.log("Successful login for: " + a.name);
            window.fbAuthorizationWrapper.profile.name = "" + a.name;
            window.fbAuthorizationWrapper.profile.id = "" + a.id;
            FB.api("/me/picture", {
                redirect: !1,
                height: "200",
                type: "normal",
                width: "200"
            }, function(a) {
                a && !a.error && (window.fbAuthorizationWrapper.profile.image = a.data.url);
                configureForFaceBookLoggedIn()
            })
        }) : "not_authorized" === a.status ? (console.log("FB App not Authorized"), window.fbAuthorizationWrapper.logout("failed")) : console.log("FB Logged out")
    },
    checkLoginState: function() {
        FB.getLoginStatus(function(a) {
            this.statusChangeCallback(a)
        })
    },
    onFinishedLogin: function() {
        FB.getLoginStatus(function(a) {
            this.statusChangeCallback(a)
        })
    },
    logout: function(a) {
        "failed" == a && (hideAllMenuControlBars(),
            window.googleInkchatAuthorization.loggedIn = !1);
        a || (hideAllMenuControlBars(), window.user = {
            name: "Guest - " + genGUID().substring(0, 8),
            authenticated: !1,
            picture: "menus/icons/user.svg"
        }, commInterface && commInterface.updateUser(), FB.logout(function(a) {}), regenLoginCGroup(window.fbAuthorizationWrapper.cGroupOld), window.googleInkchatAuthorization.loggedIn = !1)
    }
};

function configureForFaceBookLoggedIn() {
    if (!window.fbAuthorizationWrapper.loggedIn) {
        window.googleInkchatAuthorization.loggedIn = !0;
        hideAllMenuControlBars();
        var a = window.allControlGroups.signInStatusItem,
            b = window.fbAuthorizationWrapper.profile;
        window.user.name = b.name;
        window.user.picture = b.image;
        window.user.uuid = b.id;
        window.user.authenticated = !0;
        window.commInterface.updateUser();
        window.allControlButtons.secureRoom.enabled = !0;
        a.addIcon(b.image);
        window.fbAuthorizationWrapper.cGroupOld = a.controlItems;
        a.controlItems = [];
        a.addControlItem(new ControlTextField(unitsToPixels(2.5, "cm"), b.name, 12));
        b = new ControlButton(window.fbAuthorizationWrapper.logout);
        b.addIcon("menus/icons/enter_room.svg");
        a.addControlItem(b);
        a.updateWidth();
        a.collapsedButton.callbackFn = function() {
            a.callbackFn(a)
        };
        window.statusBar.updateDiv()
    }
};
var WS_SERVER_URL = "ws://inkchat.org:80/";
WebSocketInterface = function() {
    this.socket = io.connect(WS_SERVER_URL);
    this.roomID = null;
    this.updateUser();
    var a = this;
    this.socket.on("action", function(a) {
        Action.fromObject(a).perform(pageList)
    });
    this.socket.on("roomdata", loadRoomFromArrayBuffer);
    this.socket.on("getroomdata", function() {
        a.sendRoomData(roomToArrayBuffer())
    });
    this.socket.on("userlist", function(a) {
        window.userList = a
    });
    this.socket.on("switchroom", function(b) {
        b.error ? (alert(b.errorMsg), window.location.hash = "", a.joinRoomCallback && a.joinRoomCallback(!1)) :
            (a.roomID = b.roomID, window.location.hash = a.roomID, a.joinRoomCallback && a.joinRoomCallback(!0))
    });
    this.socket.on("requestRoomPassword", function(b) {
        window.user.authenticated ? (a.roomToJoin = b.roomID, b = prompt("Please enter password to Enter: "), a.roomPassword(b)) : alert("You must be signed in to enter a secure room.")
    });
    this.socket.on("syncdownuserdata", function(a) {
        console.log("received User Data from Server");
        window.user.data = a
    });
    this.socket.on("servermsg", function(a) {
        alert(a)
    });
    this.socket.on("requestUserInfo",
        function() {
            a.updateUser()
        })
};
WebSocketInterface.prototype = {
    sendAction: function(a) {
        this.roomID && this.socket && this.socket.emit("action", a.toObject())
    },
    sendRoomData: function(a) {
        this.roomID && this.socket && this.socket.emit("roomdata", a)
    },
    joinRoom: function(a, b) {
        a != this.roomID && this.socket && (this.socket.emit("joinroom", {
            roomID: a,
            user: window.user
        }), this.joinRoomCallback = b)
    },
    secureRoom: function() {
        if (window.user && window.user.authenticated)
            if (this.roomID) {
                var a = prompt("Enter a password to set room password: ");
                this.socket.emit("secureRoom", {
                    password: a
                })
            } else alert("Not in a room!");
        else alert("Not an authenticated user!")
    },
    updateUser: function() {
        this.socket && this.socket.emit("updateuser", window.user)
    },
    roomPassword: function(a) {
        this.socket && this.socket.emit("returnRoomPassword", a)
    },
    syncUpUserData: function() {
        this.roomID && this.socket && this.socket.emit("syncupuserdata", window.user.data)
    }
};
(function() {
    window.recognitionPlatform = new RecognitionPlatform
})();

function RecognitionPlatform() {
    this.recognizers = {}
}
RecognitionPlatform.prototype = {
    init: function() {},
    addRecognizer: function(a) {
        this.recognizers.push(a)
    },
    removeRecognizer: function() {},
    setRecognizerActive: function() {},
    setRecognizerInActive: function() {}
};
(function() {
    window.recognitionPlatform || console.log("Critical Error, Can't load without RecognitionPlatform!!!")
})();

function SimpleShapeRecognizer() {}
SimpleShapeRecognizer.prototype = {
    init: function() {}
};
window.user = {
    uuid: 0,
    name: "Guest - " + genGUID().substring(0, 8),
    picture: "menus/icons/user.svg",
    authenticated: !1,
    data: {}
};
window.userList = [window.user];

/*
$(".qbswbbottom > button").css("float", "none");
$(".qbswbbottom > button").css("display", "block");
$(".qbswbtop > button").css("float", "left");
$(".qbswbtop > button").css("display", "block");
*/

//top
//			this.style.cssFloat = "left"; 
//			this.style.display = "block";

			
//bottom	
//			this.style.cssFloat = "none";
//			this.style.display = "block";