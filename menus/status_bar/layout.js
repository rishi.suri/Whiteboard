function zoomInDefaultCallback() {
    updateViewportZoom(.25);
    setZoomTextBoxValue()
}

function zoomOutDefaultCallback() {
    updateViewportZoom(-.25);
    setZoomTextBoxValue()
}

function pageLeftDefaultCallback() {
    window.pageList.setActivePage(window.pageList.currPage - 1);
    setPageTextBoxValue()
}

function pageRightDefaultCallback() {
    window.pageList.setActivePage(window.pageList.currPage + 1);
    setPageTextBoxValue()
}

function pageCreateDefaultCallback() {
    var a = new ModifyPageListAction("add", pageList.currPage);
    window.pageList.addPage(window.pageList.currPage);
    window.pageList.setActivePage(window.pageList.currPage + 1);
    window.commInterface && commInterface.sendAction(a);
    setPageTextBoxValue()
}

function pageDeleteDefaultCallback() {
    var a = new ModifyPageListAction("remove", pageList.currPage);
    window.commInterface && commInterface.sendAction(a);
    window.pageList.removeCurrPage();
    setPageTextBoxValue()
}

function setZoomTextBoxValue() {
    var a = 100 * fabricCanvases[0].viewport.zoom,
        b = window.allControlTextBoxes.statusBarCurrentZoomFactor.elem;
    b && (b.value = a + "%")
}

function setPageTextBoxValue() {
    var a = window.allControlTextBoxes.statusBarCurrentPage.elem,
        b = window.allControlTextFields.statusBarTotalPages.elem;
    a && (a.value = window.pageList.currPage, b && (b.innerHTML = "/ " + window.pageList.numPages))
}

function onZoomTextChangeDefaultCallback(a, b) {
    if (a) {
        var c = a.elem;
        c && (c.onchange = function() {
            var a = parseInt(c.value);
            setViewportZoom(a / 100);
            setZoomTextBoxValue()
        })
    }
}

function onPageTextChangeDefaultCallback(a, b) {
    if (a) {
        var c = a.elem;
        c && (c.onchange = function() {
            var a = parseInt(c.value);
            window.pageList.setActivePage(a);
            setPageTextBoxValue()
        })
    }
}

function compositeLoginPrep() {}

function googleLoginNow() {
    window.googleInkchatAuthorization.googleAuthorizationRenderNow()
}

function faceBookLoginNow() {
    window.fbAuthorizationWrapper.loginButtonCallback()
}

function googlePlusLoginDefaultBuilder(a, b, c, f, e) {
    a = b.attributes;
    b = document.createElement("div");
    $.each(a, function() {
        "type" != $(this).context.nodeName && b.setAttribute($(this).context.nodeName, $(this).context.value)
    });
    b.appendChild(c);
    return [b, c]
}

function statusBuildBarUserList(a, b, c, f, e) {
    a = b.attributes;
    b = document.createElement("div");
    $.each(a, function() {
        "type" != $(this).context.nodeName && b.setAttribute($(this).context.nodeName, $(this).context.value)
    });
    var d = [];
    $.each(window.userList, function() {
        var a = document.createElement("div");
        a.width = unitsToPixels(4, "cm");
        a.style.width = unitsToPixels(4, "cm") + "px";
        a.height = unitsToPixels(.8, "cm");
        a.style.height = unitsToPixels(.8, "cm") + "px";
        a.style.border = "0px";
        a.style.width = unitsToPixels(3, "cm") + "px";
        a.style.fontSize =
            "10";
        a.style.textAlign = "center";
        a.style.paddingTop = unitsToPixels(1, "mm") + "px";
        a.style.paddingBottom = unitsToPixels(1, "mm") + "px";
        a.style.paddingLeft = unitsToPixels(2.5, "mm") + "px";
        a.style.paddingRight = unitsToPixels(1, "mm") + "px";
        a.style.marginLeft = unitsToPixels(1, "mm") + "px";
        a.style.marginRight = "0px";
        var b = document.createElement("img");
        b.src = this.picture;
        b.style.float = "left";
        b.width = unitsToPixels(.8, "cm");
        b.style.width = unitsToPixels(.8, "cm") + "px";
        b.height = unitsToPixels(.8, "cm");
        b.style.height = unitsToPixels(.8,
            "cm") + "px";
        var c = document.createElement("a");
        c.style.width = unitsToPixels(2.9, "cm");
        c.innerHTML = this.name;
        c.marginLeft = unitsToPixels(2, "mm") + "px";
        c.style.fontSize = "10px";
        c.style.textAlign = "center";
        c.href = "javascript: dummyFunction()";
        a.appendChild(b);
        a.appendChild(c);
        d.push(a)
    });
    a = e.parent;
    a.width = unitsToPixels(4, "cm");
    a.elem.style.left = "0px";
    a.xpos = 0;
    a.elem.style.width = unitsToPixels(4, "cm") + "px";
    a.elem.style.marginLeft = "0px";
    a.elem.style.marginRight = "0px";
    a.position = "absolute";
    a.elem.position = "absolute";
    a.gravity = "bottom";
    a.widthfit = !1;
    a.rows = d.length - 1;
    e.marginLeft = 0;
    e.marginRight = 0;
    b.style.marginLeft = "0px";
    b.style.marginRight = "0px";
    b.height = (unitsToPixels(1, "cm") + 1) * d.length;
    b.style.height = (unitsToPixels(1, "cm") + 1) * d.length + "px";
    a.ypos = window.defaultMenuSystemStyles.rowHeight;
    b.width = unitsToPixels(4, "cm");
    b.style.width = unitsToPixels(4, "cm") + "px";
    b.style.border = "0px";
    b.border = 0;
    $.each(d, function() {
        b.appendChild(this)
    });
    hideAllMenuControlBars();
    return [b, c]
};