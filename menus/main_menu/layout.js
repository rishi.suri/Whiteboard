function genLineWidthButton(b, c, a, d) {
	var f = !1;
	"customSlider" == b && (b = currentBrush.width, f = !0);
	f && (a = c.attributes, c = document.createElement("div"), $.each(a, function () {
			"type" != $(this).context.nodeName && c.setAttribute($(this).context.nodeName, $(this).context.value)
		}));
	getControlGroupByName("widthMenuDropDownLocal").groupWidth = unitsToPixels(3, "cm");
	a = document.createElement("canvas");
	var e = a.getContext("2d");
	a.width = unitsToPixels(2.8, "cm");
	a.height = unitsToPixels(.8, "cm");
	c.style.marginLeft = unitsToPixels(0,
			"mm") + "px";
	c.style.marginRight = unitsToPixels(0, "mm") + "px";
	f ? (c.style.width = unitsToPixels(2.8, "cm") + "px", c.style.height = unitsToPixels(.8, "cm") + "px") : (c.style.width = unitsToPixels(3, "cm") + "px", c.style.height = unitsToPixels(1, "cm") + "px");
	c.style.marginTop = unitsToPixels(0, "cm") + "px";
	c.style.marginBottom = unitsToPixels(0, "cm") + "px";
	c.style.border = "0px";
	c.style.float = "none";
	e.lineWidth = b;
	e.lineCap = "butt";
	d && (e.strokeStyle = window.defaultMenuSystemStyles.menuForgroundColor, e.fillStyle = window.defaultMenuSystemStyles.menuForgroundColor,
		e.beginPath(), e.moveTo(unitsToPixels(1, "mm"), .3 * a.height), e.lineTo(unitsToPixels(1, "mm"), .7 * a.height), e.lineTo(unitsToPixels(3, "mm"), .5 * a.height), e.lineTo(unitsToPixels(1, "mm"), .3 * a.height), e.lineTo(0, 0), e.closePath(), e.fill());
	e.strokeStyle = currentBrush.color;
	e.fillStyle = currentBrush.color;
	e.beginPath();
	e.moveTo(unitsToPixels(5.5, "mm"), a.height / 2);
	e.lineTo(a.width, a.height / 2);
	e.stroke();
	f && (document.createElement("div"), b = document.createElement("input"), c.style.border = unitsToPixels(1, "mm") + "px solid " +
			window.defaultMenuSystemStyles.menuBackgroundColor, b.type = "range", b.min = "1", b.max = "50", b.value = currentBrush.width, b.id = "widthCustomSlider", b.style.position = "absolute", b.style.top = 4 * unitsToPixels(1, "cm") - unitsToPixels(.5, "mm") + "px", b.style.left = unitsToPixels(6, "mm") + "px", b.style.width = unitsToPixels(2.3, "cm") + "px", b.style.height = unitsToPixels(1, "cm") + "px", b.style.marginLeft = "0px", d = document.createElement("div"), a.id = "widthCustomCanvasSlider", d.appendChild(a), d.appendChild(b), a = d);
	hideAllMenuControlBars();
	return [c, a, 0]
}
function brushTypesSelectionFunction() {
	switch (window.currentBrush.type) {
	case InkBrush.types.drawing:
		window.allControlButtons.brushTypesDrawingToolMenuItem.setSelected(!0);
		break;
	case InkBrush.types.stroke_eraser:
		window.allControlButtons.brushTypesStrokeEraserMenuItem.setSelected(!0);
		break;
	case InkBrush.types.pointer:
		window.allControlButtons.brushTypesPointerMenuItem.setSelected(!0);
		break;
	case InkBrush.types.selection:
		window.allControlButtons.brushTypesSelectionMenuItem.setSelected(!0);
		break;
	case InkBrush.types.line:
		window.allControlButtons.brushTypesLineToolMenuItem.setSelected(!0);
		break;
	case InkBrush.types.text:
		window.allControlButtons.brushTypesTextMenuItem.setSelected(!0);
		break;
	case InkBrush.types.math:
		window.allControlButtons.brushTypesMathMenuItem.setSelected(!0);
		break;
	case InkBrush.types.move:
		window.allControlButtons.brushTypesMoveMenuItem.setSelected(!0)
	}
}
function brushSettingsWidthSelection() {
	switch (window.currentBrush.width) {
	case 1:
		window.allControlButtons.widthSelectionOne.setSelected(!0);
		break;
	case 5:
		window.allControlButtons.widthSelectionFive.setSelected(!0);
		break;
	case 10:
		window.allControlButtons.widthSelectionTen.setSelected(!0);
		break;
	case 15:
		window.allControlButtons.widthSelectionFifteen.setSelected(!0);
		break;
	default:
		window.allControlButtons.widthSelectionCustom.setSelected(!0)
	}
}
function transpSettingsWidthSelection() {
	switch (window.currentBrush.transparency) {
	case .6:
		window.allControlButtons.transparencySelection40.setSelected(!0);
		break;
	case .4:
		window.allControlButtons.transparencySelection60.setSelected(!0);
		break;
	case .2:
		window.allControlButtons.transparencySelection80.setSelected(!0);
		break;
	case 0:
		window.allControlButtons.transparencySelection100.setSelected(!0);
		break;
	default:
		window.allControlButtons.transparencySelectionCustom.setSelected(!0)
	}
}
function unselectWidthSelection() {
	var b = window.allControlButtons.widthSelectionFive,
	c = window.allControlButtons.widthSelectionTen,
	a = window.allControlButtons.widthSelectionFifteen,
	d = function (a, b, c) {
		a.strokeStyle = currentBrush.color;
		a.fillStyle = currentBrush.color;
		a.clearRect(0, 0, unitsToPixels(2.8, "cm"), unitsToPixels(.8, "cm"));
		a.lineWidth = c;
		a.lineCap = "butt";
		a.beginPath();
		a.moveTo(unitsToPixels(5.5, "mm"), b.height / 2);
		a.lineTo(b.width, b.height / 2);
		a.stroke()
	},
	f = document.getElementById(window.allControlButtons.widthSelectionOne.id);
	$(f).children("canvas").each(function () {
		d(this.getContext("2d"), this, 2)
	});
	f = document.getElementById(b.id);
	$(f).children("canvas").each(function () {
		d(this.getContext("2d"), this, 5)
	});
	f = document.getElementById(c.id);
	$(f).children("canvas").each(function () {
		d(this.getContext("2d"), this, 10)
	});
	f = document.getElementById(a.id);
	$(f).children("canvas").each(function () {
		d(this.getContext("2d"), this, 15)
	})
}
function widthMenuDropDownFunction() {
	var b = function () {
		var b = document.getElementById("widthCustomCanvasSlider"),
		a = b.getContext("2d");
		a.clearRect(0, 0, unitsToPixels(2.8, "cm"), unitsToPixels(.8, "cm"));
		a.lineWidth = this.value;
		a.lineCap = "butt";
		a.strokeStyle = window.defaultMenuSystemStyles.menuForgroundColor;
		a.fillStyle = window.defaultMenuSystemStyles.menuForgroundColor;
		a.beginPath();
		a.moveTo(unitsToPixels(1, "mm"), .3 * b.height);
		a.lineTo(unitsToPixels(1, "mm"), .7 * b.height);
		a.lineTo(unitsToPixels(3, "mm"), .5 * b.height);
		a.lineTo(unitsToPixels(1, "mm"), .3 * b.height);
		a.closePath();
		a.fill();
		a.strokeStyle = currentBrush.color;
		a.fillStyle = currentBrush.color;
		a.beginPath();
		a.moveTo(unitsToPixels(5.5, "mm"), b.height / 2);
		a.lineTo(b.width, b.height / 2);
		a.stroke();
		setLineWidth(this.value);
		unselectWidthSelection()
	};
	if (8 < msieversion())
		$("#widthCustomSlider").on("change", b);
	else
		$("#widthCustomSlider").on("input", b)
}
function widthMenuDropDownLocal(b) {
	widthMenuDropDown(b)
}
function unselectTransparencySelection() {
	var b = window.allControlButtons.transparencySelection60,
	c = window.allControlButtons.transparencySelection80,
	a = window.allControlButtons.transparencySelection100,
	d = function (a, b, c) {
		drawTranspBackgrounds(a, b, unitsToPixels(.25, "cm"));
		a.clearRect(0, 0, unitsToPixels(5.5, "mm"), b.height);
		a.strokeStyle = currentBrush.color;
		a.fillStyle = currentBrush.color;
		a.lineCap = "butt";
		a.lineWidth = 50;
		a.globalAlpha = c / 100;
		a.beginPath();
		a.moveTo(unitsToPixels(5.5, "mm"), b.height / 2);
		a.lineTo(b.width,
			b.height / 2);
		a.stroke()
	},
	f = document.getElementById(window.allControlButtons.transparencySelection40.id);
	$(f).children("canvas").each(function () {
		d(this.getContext("2d"), this, 40)
	});
	f = document.getElementById(b.id);
	$(f).children("canvas").each(function () {
		d(this.getContext("2d"), this, 60)
	});
	f = document.getElementById(c.id);
	$(f).children("canvas").each(function () {
		d(this.getContext("2d"), this, 80)
	});
	f = document.getElementById(a.id);
	$(f).children("canvas").each(function () {
		d(this.getContext("2d"), this, 100)
	})
}
function transpMenuDropDownFunction() {
	var b = function () {
		var b = document.getElementById("transpCustomCanvasSlider"),
		a = b.getContext("2d");
		a.clearRect(0, 0, unitsToPixels(2.8, "cm"), unitsToPixels(.8, "cm"));
		drawTranspBackgrounds(a, b, unitsToPixels(.25, "cm"));
		a.fillStyle = currentBrush.color;
		a.clearRect(0, 0, unitsToPixels(5.5, "mm"), b.height);
		a.lineWidth = this.value;
		a.lineCap = "butt";
		a.strokeStyle = window.defaultMenuSystemStyles.menuForgroundColor;
		a.fillStyle = window.defaultMenuSystemStyles.menuForgroundColor;
		a.beginPath();
		a.moveTo(unitsToPixels(1, "mm"), .3 * b.height);
		a.lineTo(unitsToPixels(1, "mm"), .7 * b.height);
		a.lineTo(unitsToPixels(3, "mm"), .5 * b.height);
		a.lineTo(unitsToPixels(1, "mm"), .3 * b.height);
		a.closePath();
		a.fill();
		a.fillStyle = currentBrush.color;
		a.strokeStyle = currentBrush.color;
		a.lineWidth = unitsToPixels(2.8, "cm");
		a.globalAlpha = this.value / 100;
		a.lineCap = "butt";
		a.beginPath();
		a.moveTo(unitsToPixels(5.5, "mm"), b.height / 2);
		a.lineTo(b.width, b.height / 2);
		a.stroke();
		setTransparency(1 - this.value / 100);
		unselectTransparencySelection()
	};
	if (8 < msieversion())
		$("#transpCustomSlider").on("change", b);
	else
		$("#transpCustomSlider").on("input", b)
}
function genLineTransButton(b, c, a, d) {
	var f = !1;
	"customSlider" == b && (b = currentBrush.opacity, f = !0);
	if (f) {
		var e = c.attributes;
		c = document.createElement("div");
		$.each(e, function () {
			"type" != $(this).context.nodeName && c.setAttribute($(this).context.nodeName, $(this).context.value)
		})
	}
	e = document.createElement("canvas");
	a = e.getContext("2d");
	e.width = unitsToPixels(2.8, "cm");
	e.height = unitsToPixels(.8, "cm");
	f ? (c.style.width = unitsToPixels(2.8, "cm") + "px", c.style.height = unitsToPixels(.8, "cm") + "px") : (c.style.width = unitsToPixels(3,
				"cm") + "px", c.style.height = unitsToPixels(1, "cm") + "px");
	c.style.marginLeft = unitsToPixels(0, "mm") + "px";
	c.style.marginRight = unitsToPixels(0, "mm") + "px";
	c.style.marginTop = unitsToPixels(0, "cm") + "px";
	c.style.marginBottom = unitsToPixels(0, "cm") + "px";
	c.style.border = "0px";
	c.style.position = "relative";
	c.style.float = "none";
	getControlGroupByName("transparencyMenuDropDownLocal").groupWidth = unitsToPixels(3, "cm");
	drawTranspBackgrounds(a, e, unitsToPixels(.25, "cm"));
	a.clearRect(0, 0, unitsToPixels(5.5, "mm"), e.height);
	a.lineCap =
		"butt";
	d && (a.strokeStyle = window.defaultMenuSystemStyles.menuForgroundColor, a.fillStyle = window.defaultMenuSystemStyles.menuForgroundColor, a.beginPath(), a.moveTo(unitsToPixels(1, "mm"), .3 * e.height), a.lineTo(unitsToPixels(1, "mm"), .7 * e.height), a.lineTo(unitsToPixels(3, "mm"), .5 * e.height), a.lineTo(unitsToPixels(1, "mm"), .3 * e.height), a.closePath(), a.fill());
	a.strokeStyle = currentBrush.color;
	a.fillStyle = currentBrush.color;
	a.lineWidth = 50;
	a.globalAlpha = f ? 1 - currentBrush.transparency : b / 100;
	a.beginPath();
	a.moveTo(unitsToPixels(5.5,
			"mm"), e.height / 2);
	a.lineTo(e.width, e.height / 2);
	a.stroke();
	a = e;
	f ? (document.createElement("div"), b = document.createElement("input"), c.style.border = unitsToPixels(1, "mm") + "px solid " + window.defaultMenuSystemStyles.menuBackgroundColor, b.type = "range", b.min = "0", b.max = "100", b.value = 100 * (1 - currentBrush.transparency), b.id = "transpCustomSlider", e.id = "transpCustomCanvasSlider", b.style.position = "absolute", b.style.top = unitsToPixels(0, "mm") + "px", b.style.left = unitsToPixels(4.5, "mm") + "px", b.style.width = unitsToPixels(2.3,
				"cm") + "px", b.style.height = unitsToPixels(.8, "cm") + "px", d = document.createElement("div"), d.appendChild(e), d.appendChild(b), a = d, $("#transpCustomSlider").on("input", function () {
			console.log(this.value)
		})) : a = e;
	hideAllMenuControlBars();
	return [c, a, 0]
}
function paletteButtonColorSet(b, c, a, d, f) {
	a.style.backgroundColor = currentBrush.color;
	a.id = "paletteButtonIconColor";
	window.paletteButtonColorButtonId = a.id;
	return [c, a]
}
function genPaletteSelector(b, c, a, d, f) {
	d = document.createElement("img");
	d.src = "menus/icons/advanced_color_select.png";
	d.width = unitsToPixels(3.7, "cm") / 8 * 6;
	d.height = 120 / 297 * d.width;
	d.style.width = d.width + "px";
	d.style.height = d.height + "px";
	b = document.createElement("button");
	b.style.width = d.style.width;
	b.style.height = d.style.height;
	b.width = d.width;
	b.height = d.height;
	b.style.paddingLeft = "0px";
	b.style.paddingRight = "0px";
	b.style.paddingTop = "0px";
	b.style.paddingBottom = "0px";
	b.style.marginLeft = unitsToPixels(3.7,
			"cm") / 8 + "px";
	b.style.marginTop = unitsToPixels(1, "mm") + "px";
	b.style.border = "none";
	b.style.outline = "none";
	b.style.backgroundColor = "rgb(235,235,235)";
	b.appendChild(d);
	window.advancedPaletteButton = b;
	var e = c.attributes;
	parentElem = document.getElementById(f.parent.parent.id);
	c = document.createElement("div");
	$.each(e, function () {
		"type" != $(this).context.nodeName && c.setAttribute($(this).context.nodeName, $(this).context.value)
	});
	var e = -1,
	g = f.parent.elem.offsetLeft;
	g + unitsToPixels(3.7, "cm") > window.innerWidth && (e =
			g + unitsToPixels(3.9, "cm") - window.innerWidth);
	c.style.width = unitsToPixels(3.7, "cm") + "px";
	c.width = unitsToPixels(3.7, "cm");
	c.style.height = 8 * unitsToPixels(7, "mm") + unitsToPixels(3, "mm") + d.height + "px";
	c.height = 8 * unitsToPixels(7, "mm") + unitsToPixels(3, "mm") + d.height;
	c.style.marginLeft = "0px";
	c.style.marginRight = "0px";
	c.style.marginTop = unitsToPixels(0, "mm") + "px";
	c.style.marginBottom = unitsToPixels(0, "cm") + "px";
	c.style.float = "right";
	c.style.position = "absolute";
	f.parent.elem.backgroundColor = "rgba(255,255,255,255)";
	f.parent.width = 0;
	f.parent.rows = 1;
	f.parent.elem.width = "0px";
	f.parent.elem.height = "0px";
	0 < e && (c.style.left = "-" + e + "px");
	for (f = 0; 8 > f; f++)
		for (d = -2; 3 > d; d++) {
			e = document.createElement("button");
			e.id = genGUID();
			var g = "#000",
			h = g = 0,
			k = 0,
			l = 0;
			switch (f) {
			case 0:
				k = h = g = 0;
				break;
			case 1:
				g = 255;
				l = k = h = 0;
				break;
			case 2:
				g = 255;
				h = 127;
				k = 0;
				l = 29.88 / 360;
				break;
			case 3:
				h = g = 255;
				k = 0;
				l = 60 / 360;
				break;
			case 4:
				g = 0;
				h = 255;
				k = 0;
				l = 120 / 360;
				break;
			case 5:
				h = g = 0;
				k = 255;
				l = 240 / 360;
				break;
			case 6:
				g = 75;
				h = 0;
				k = 130;
				l = 275 / 360;
				break;
			case 7:
				g = 143,
				h = 0,
				k = 255,
				l = 300 / 360
			}
			var m;
			switch (d) {
			case -2:
				m = 0 == f ? HSVtoRGB(255, 0, 0) : HSVtoRGB(l, 1, .5);
				break;
			case -1:
				m = 0 == f ? HSVtoRGB(255, 0, .25) : HSVtoRGB(l, 1, .75);
				break;
			case 0:
				m = 0 == f ? HSVtoRGB(255, 0, .5) : HSVtoRGB(l, 1, 1);
				break;
			case 1:
				m = 0 == f ? HSVtoRGB(255, 0, .75) : HSVtoRGB(l, .5, 1);
				break;
			case 2:
				m = 0 == f ? HSVtoRGB(255, 0, 1) : HSVtoRGB(l, .25, 1)
			}
			0 > g && (g = 0);
			0 > h && (h = 0);
			0 > k && (k = 0);
			255 < g && (g = 255);
			255 < h && (h = 255);
			255 < k && (k = 255);
			g = rgb2hex(m.r, m.g, m.b);
			e.style.backgroundColor = g;
			e.style.width = unitsToPixels(.6, "cm") + "px";
			e.style.marginLeft = unitsToPixels(1, "mm") + "px";
			e.style.marginTop = unitsToPixels(1, "mm") + "px";
			e.style.height = unitsToPixels(.6, "cm") + "px";
			e.style.border = "none";
			e.style.float = "left";
			window.currentBrush.color == "rgb(" + m.r + ", " + m.g + ", " + m.b + ")" && (e.style.border = unitsToPixels(1, "mm") + "px solid", e.style.borderColor = "rgb(0, 0, 0)");
			c.appendChild(e);
			$(e).click(function () {
				hideAllMenuControlBars();
				setColor(this.style.backgroundColor);
				document.getElementById(window.paletteButtonColorButtonId).style.backgroundColor = this.style.backgroundColor
			})
		}
	c.appendChild(b);
	window.activePaletteId = c.id;
	window.advancedPaletteActive = !1;
	hideAllMenuControlBars();
	$(b).on("click", function () {
		window.advancedPaletteActive || generateAdvancedPalette()
	});
	return [c, a]
}
function generateAdvancedPalette() {
	window.advancedPaletteActive = !0;
	var b = document.getElementById(window.activePaletteId),
	c = document.createElement("div"),
	a = b.getBoundingClientRect();
	c.style.width = unitsToPixels(6, "cm") + "px";
	c.style.height = b.style.height;
	c.style.position = "relative";
	0 > a.left - unitsToPixels(6.2, "cm") ? c.style.left = "-" + a.left + "px" : c.style.left = "-" + unitsToPixels(6.2, "cm") + "px";
	c.style.top = "-" + b.style.height;
	c.style.backgroundColor = "rgb(235,235,235)";
	c.style.border = unitsToPixels(1, "mm") + "px solid";
	c.style.borderColor = "rgb(235,235,235)";
	c.id = "advancedPaletteControls";
	var d = document.createElement("canvas"),
	a = document.createElement("input");
	d.id = "advancedPaletteHSVCanvas";
	d.width = unitsToPixels(5.6, "cm");
	d.height = unitsToPixels(5.6, "cm");
	d.style.width = d.width + "px";
	d.style.height = d.height + "px";
	d.style.marginLeft = unitsToPixels(2, "mm") + "px";
	d.style.marginTop = unitsToPixels(1, "mm") + "px";
	d = processCanvasToHSVCanvas(d, 1);
	c.appendChild(d);
	$(d).on("click", function (a) {
		a = getHexFromPaletteHSVAt(d, a.pageX, a.pageY);
		window.setColor(a);
		var b = document.getElementById(window.paletteButtonColorButtonId);
		try {
			b.style.backgroundColor = a
		} catch (c) {}
		hideAllMenuControlBars()
	});
	var f = document.createElement("div");
	a.id = "advancedPaletteHSVValueSlider";
	a.type = "range";
	a.min = 0;
	a.max = 100;
	a.value = 100;
	a.style.width = unitsToPixels(5.6, "cm") + "px";
	a.style.height = window.advancedPaletteButton.style.height;
	f.style.backgroundColor = "#DDD";
	f.style.width = unitsToPixels(5.6, "cm") + "px";
	f.style.height = window.advancedPaletteButton.style.height;
	f.style.marginLeft =
		unitsToPixels(2, "mm") + "px";
	f.style.marginTop = unitsToPixels(1, "mm") + "px";
	f.appendChild(a);
	c.appendChild(f);
	$(a).on("input", function () {
		processCanvasToHSVCanvas(d, this.value / 100)
	});
	b.appendChild(c)
}
function getHexFromPaletteHSVAt(b, c, a) {
	var d = document.getElementById(b.id).getBoundingClientRect();
	c -= d.left;
	a -= d.top;
	p = b.getContext("2d").getImageData(c, a, 1, 1).data;
	return rgb2hex(p[0], p[1], p[2])
}
function processCanvasToHSVCanvas(b, c) {
	for (var a = b.getContext("2d"), d = a.createImageData(b.width, b.height), f = 0, e = 0; e < b.height; e++)
		for (var g = b.width - 1; 0 <= g; g--) {
			var h = HSVtoRGB(e / b.width, g / (b.height + 1), c);
			d.data[f++] = h.r;
			d.data[f++] = h.g;
			d.data[f++] = h.b;
			d.data[f++] = 255
		}
	a.putImageData(d, 0, 0);
	return b
}
function rgb2hex(b, c, a) {
	return "#" + (16777216 + (a | c << 8 | b << 16)).toString(16).slice(1)
}
function HSVtoRGB(b, c, a) {
	var d,
	f,
	e,
	g,
	h,
	k;
	b && void 0 === c && void 0 === a && (c = b.s, a = b.v, b = b.h);
	g = Math.floor(6 * b);
	h = 6 * b - g;
	b = a * (1 - c);
	k = a * (1 - h * c);
	c = a * (1 - (1 - h) * c);
	switch (g % 6) {
	case 0:
		d = a;
		f = c;
		e = b;
		break;
	case 1:
		d = k;
		f = a;
		e = b;
		break;
	case 2:
		d = b;
		f = a;
		e = c;
		break;
	case 3:
		d = b;
		f = k;
		e = a;
		break;
	case 4:
		d = c;
		f = b;
		e = a;
		break;
	case 5:
		d = a,
		f = b,
		e = k
	}
	return {
		r: Math.floor(255 * d),
		g: Math.floor(255 * f),
		b: Math.floor(255 * e)
	}
}
function loadSettingsButtonDefaultCallback() {
	var b = new ControlBar(unitsToPixels(.1, "cm") * window.menuSystemScaleFactor, unitsToPixels(1.5, "cm") * window.menuSystemScaleFactor, unitsToPixels(5.2, "cm") * window.menuSystemScaleFactor, 3),
	c = new ControlButton(function () {});
	c.acontent = function (a, b, c, g) {
		a = b.attributes;
		b = document.createElement("div");
		$.each(a, function () {
			"type" != $(this).context.nodeName && b.setAttribute($(this).context.nodeName, $(this).context.value)
		});
		a = document.createElement("input");
		a.id = "mainMenusScaleControlSlider";
		a.type = "range";
		a.min = 50;
		a.max = 150;
		a.value = 100 * window.menuSystemScaleFactor;
		a.style.width = unitsToPixels(2.8, "cm") * window.menuSystemScaleFactor + "px";
		a.style.height = unitsToPixels(.8, "cm") * window.menuSystemScaleFactor + "px";
		b.style.backgroundColor = "#DDD";
		b.style.width = unitsToPixels(2.8, "cm") * window.menuSystemScaleFactor + "px";
		b.style.height = unitsToPixels(.8, "cm") * window.menuSystemScaleFactor + "px";
		b.appendChild(a);
		return [b, c]
	};
	var a = new ControlTextField(unitsToPixels(1.5, "cm") * window.menuSystemScaleFactor,
			" Settings", 16, b);
	a.paddingLeft = unitsToPixels(3, "mm");
	b.addControlItem(a);
	a = new ControlSpacer(unitsToPixels(2.1, "cm") * window.menuSystemScaleFactor, !1, b);
	b.addControlItem(a);
	a = new ControlButton(function () {
			window.settingsControlMenu.destroy()
		});
	a.width = unitsToPixels(1, "cm") * window.menuSystemScaleFactor;
	a.addIcon("menus/icons/img_icon_close.svg");
	a.paddingLeft = 0;
	a.paddingRight = 0;
	a.marginLeft = 0;
	a.marginRight = 0;
	a.content.style.width = unitsToPixels(1, "cm") * window.menuSystemScaleFactor + "px";
	a.content.style.height =
		unitsToPixels(1, "cm") * window.menuSystemScaleFactor + "px";
	a.contentWidth = unitsToPixels(.8, "cm") * window.menuSystemScaleFactor;
	a.contentHeight = unitsToPixels(.8, "cm") * window.menuSystemScaleFactor;
	b.addControlItem(a);
	a = new ControlTextField(unitsToPixels(1.6, "cm") * window.menuSystemScaleFactor, "Menu Scale x" + window.menuSystemScaleFactor, 10);
	b.addControlItem(a);
	b.addControlItem(c);
	c = new ControlTextField(unitsToPixels(2, "cm") * window.menuSystemScaleFactor, window.INKCHAT_CURRENT_VERSION, 14, b);
	a = new ControlTextField(unitsToPixels(1,
				"cm") * window.menuSystemScaleFactor, '<a href="http://inkchat.org/about.html">About/Contact</a>', 14, b);
	b.addControlItem(a);
	a = new ControlSpacer(unitsToPixels(1.6, "cm") * window.menuSystemScaleFactor, !1, b);
	b.addControlItem(a);
	b.addControlItem(c);
	b.initShow();
	window.settingsControlMenu = b;
	$("#mainMenusScaleControlSlider").on("input", function (a) {
		window.menuSystemScaleFactor = this.value / 100;
		window.mainControlBar.rowsfit = !0;
		window.statusBar.rowsfit = !0;
		window.mainControlBar.updateDiv();
		window.statusBar.updateDiv()
	})
};
